#pragma once

#include "nativeRunLib/common.h"
#include "nativeRunLib/runlib.h"

#include "CppUnitTestAssert.h"

namespace Microsoft::VisualStudio::CppUnitTestFramework {
    template <>
    std::wstring ToString(const NRunLib::TResult::EOutcome& outcome) {
        std::stringstream ss;
        ss << outcome;
        return StdStringToStdWstring(ss.str());
    }
}
