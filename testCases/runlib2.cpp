#include "stdafx.h"

#include <nativeRunLib/common.h>
#include <nativeRunLib/runlib2.h>
#include <nativeRunLib/exception.h>
#include <nativeRunLib/filesystem.h>

#include "CppUnitTest.h"

#include <fstream>
#include <numeric>
#include <string>
#include <thread>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace NRunLib2;

namespace Microsoft {
    namespace VisualStudio {
        namespace CppUnitTestFramework
        {
            template<>
            std::wstring ToString<EOutcome>(const EOutcome& t) {
                std::stringstream ss;
                ss << t;
                return StdStringToStdWstring(ss.str());
            }
        }
    }
}

namespace NRunLib2Tests {
    TEST_CLASS(TExitCodeTest)
    {
        TEST_METHOD(Test0)
        {
            TCommand cmd{"testApp.exe return_0"};
            auto res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);
        }

        TEST_METHOD(Test42)
        {
            TCommand cmd{"testApp.exe return_42"};
            auto res = Run(cmd);
            Assert::AreEqual(42U, res.ExitCode);
        }

        TEST_METHOD(Test4000000000)
        {
            TCommand cmd{"testApp.exe return_4000000000"};
            auto res = Run(cmd);
            Assert::AreEqual(4000000000U, res.ExitCode);
        }
    };

    TEST_CLASS(TBadExePathTest)
    {
        TEST_METHOD(Empty)
        {
            Assert::ExpectException<TWindowsError>([]() { Run(TCommand{""}); });
        }

        TEST_METHOD(NotExists)
        {
            Assert::ExpectException<TWindowsError>([]() { Run(TCommand{"does_not_exist.exe"}); });
        }
    };

    TEST_CLASS(TSubprocessTest)
    {
        TEST_METHOD(TestPass)
        {
            auto res = Run(TCommand{"testApp.exe"});
            Assert::AreEqual(0U, res.ExitCode);
            Assert::IsFalse(res.ProcessLimitHit);
        }

        TEST_METHOD(TestCalc)
        {
            auto res = Run(TCommand{"testApp.exe run_calc"});
            Assert::AreNotEqual(0U, res.ExitCode);
            Assert::IsTrue(res.ProcessLimitHit);
        }

        TEST_METHOD(TestPause)
        {
            auto res = Run(TCommand{"testApp.exe run_pause"});
            Assert::AreNotEqual(0U, res.ExitCode);
            Assert::IsTrue(res.ProcessLimitHit);
        }

        TEST_METHOD(TestPauseEnv)
        {
            TCommand cmd{"testApp.exe run_pause"};
            cmd.EnvironmentMode = EEnvironmentMode::Override;

            cmd.EnvVariables = {};
            auto res = Run(cmd);
            Assert::IsFalse(res.ProcessLimitHit);

            cmd.EnvVariables = {{"ComSpec", GetEnvVariable(L"ComSpec")}};
            res = Run(cmd);
            Assert::IsTrue(res.ProcessLimitHit);
        }
    };

    TEST_CLASS(TTimeLimitTest)
    {
        TEST_METHOD(TestInfinite)
        {
            auto res = Run(TCommand{"testApp.exe infinite_loop"});
            Assert::AreEqual(0U, res.ExitCode);
            Assert::IsTrue(res.TimeLimitHit);
        }
    };

    TEST_CLASS(TMemoryLimitTest)
    {
        TEST_METHOD(TestVallocCommit)
        {
            TCommand cmd{"testApp.exe allocate_valloc_commit_100000000"};
            TResult res;

            res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);
            Assert::IsFalse(res.MemoryLimitHit);

            cmd.MemoryLimit = 64 * 1024 * 1024;
            res = Run(cmd);
            Assert::IsTrue(res.MemoryLimitHit);

            cmd.MemoryAccountingMode = EMemoryAccountingMode::WorkingSetSize;
            res = Run(cmd);
            Assert::IsFalse(res.MemoryLimitHit);
        }
    };

    TEST_CLASS(TIdlenessLimitTest)
    {
        TEST_METHOD(TestInfiniteRead)
        {
            TCommand cmd{"testApp.exe a_plus_b"};

            auto res = Run(cmd);
            Assert::IsTrue(res.IdlenessLimitHit);
            Assert::IsFalse(res.TimeLimitHit);
        }

        TEST_METHOD(TestAlmostIdle)
        {
            TCommand cmd{"testApp.exe almost_idle"};
            cmd.IdlenessLimit = true;
            cmd.TimeLimit = 100;
            auto res = Run(cmd);
            Assert::IsTrue(res.IdlenessLimitHit);
            Assert::IsFalse(res.TimeLimitHit);
        }
    };

    TEST_CLASS(TEnvironmentTest)
    {
        TEST_METHOD(TestEnv)
        {
            TCommand cmd{"testApp.exe return_getenv_VAR"};
            TResult res;

            res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);
            
            cmd.EnvironmentMode = EEnvironmentMode::Override;
            res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);

            cmd.EnvVariables.push_back({"VAR", "42"});
            res = Run(cmd);
            Assert::AreEqual(42U, res.ExitCode);
        }
    };

    TEST_CLASS(TRuntimeErrorTest)
    {
        TEST_METHOD(TestException)
        {
            TCommand cmd{"testApp.exe exception"};
            TResult res;

            res = Run(cmd);
            Assert::IsFalse(res.IdlenessLimitHit);
            Assert::AreNotEqual(0U, res.ExitCode);
        }
    };

    TEST_CLASS(TNetworkTest)
    {
        TEST_METHOD(TestPing)
        {
            TCommand cmd{"ping -n 1 localhost"};
            TResult res;

            cmd.SecurityMode = ESecurityMode::LowIntegrity;
            res = Run(cmd);
            Assert::AreNotEqual(0U, res.ExitCode);

            cmd.SecurityMode = ESecurityMode::None;
            res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);
        }
    };

    TEST_CLASS(TRedirectionTest)
    {
        TEST_METHOD(TestSimple)
        {
            TCommand cmd{"testApp.exe a_plus_b"};
            TResult res;
            {
                std::ofstream f{"input.txt"};
                f << 2 << ' ' << 2 << '\n';
            }
            cmd.StdInput.SetFile("input.txt");
            cmd.StdOutput.SetFile("output.txt");

            res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);

            {
                std::ifstream f{"output.txt"};
                int ans;
                f >> ans;
                Assert::IsTrue(bool(f));
                Assert::AreEqual(4, ans);
            }
        }

        TEST_METHOD(TestMixStdoutStderr)
        {
            TCommand cmd{"testApp.exe mix_stdout_stderr"};
            cmd.StdOutput.SetFile("stdouterr.txt");
            cmd.StdError.Mode = ERedirectMode::StdOutput;
            auto res = Run(cmd);
            {
                std::ifstream f{"stdouterr.txt"};
                std::string line;
                std::getline(f, line);
                Assert::AreEqual("first", line.c_str());
                std::getline(f, line);
                Assert::AreEqual("second", line.c_str());
                std::getline(f, line);
                Assert::AreEqual("third", line.c_str());
            }
        }
    };

    TEST_CLASS(TFileSystemTest)
    {
        TEST_METHOD(TestCreate)
        {
            TCommand cmd{"testApp.exe create_file output.txt"};
            auto res = Run(cmd);
            Assert::AreEqual(0xBADu, res.ExitCode);

            cmd.SecurityMode = ESecurityMode::None;
            res = Run(cmd);
            Assert::AreEqual(0x0u, res.ExitCode);
        }
    };

    TEST_CLASS(TOutcomeTest)
    {
        TEST_METHOD(Test)
        {
            TResult res;
            Assert::AreEqual(EOutcome::Ok, res.GetOutcome());

            res.ExitCode = 1;
            Assert::AreEqual(EOutcome::RuntimeError, res.GetOutcome());

            res.TimeLimitHit = true;
            Assert::AreEqual(EOutcome::TimeLimitExceeded, res.GetOutcome());
        }
    };

    TEST_CLASS(TCanRunTest)
    {
        TEST_METHOD(Test)
        {
            Assert::IsTrue(CanRun(TCommand{"testApp.exe"}));
            Assert::IsFalse(CanRun(TCommand{"does_not_exist.exe"}));
        }
    };

    TEST_CLASS(TMultithreadedTest)
    {
        TEST_METHOD(TestRedirection)
        {
            auto run = [](int n, bool* opened) {
                std::string ofn = "output_" + std::to_string(n) + ".txt";
                TCommand cmd{"testApp.exe run_for_300"};
                cmd.StdOutput.SetFile(ofn);

                TResult res = Run(cmd);
                if (res.ExitCode == 0) {
                    std::ifstream f{ofn};
                    *opened = bool(f);
                }
            };

            bool o1 = false;
            bool o2 = false;
            std::thread t1(run, 1, &o1);
            std::thread t2(run, 2, &o2);
            t1.join();
            t2.join();
            Assert::IsTrue(o1);
            Assert::IsTrue(o2);
        }
    };

    TEST_CLASS(TAffinityMaskTest)
    {
        TEST_METHOD(SmokeTest)
        {
            TCommand cmd{"testApp.exe return_0"};
            cmd.AffinityMask = 1;
            TResult res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);

            cmd.AffinityMask = uint64_t(1) << 63;
            Assert::ExpectException<TConfigurationError>([&cmd]() { Run(cmd); });
        }
    };

    TEST_CLASS(TProcessPriorityTest)
    {
        TEST_METHOD(SmokeTest)
        {
            TCommand cmd{"testApp.exe return_0"};
            cmd.ProcessPriority = EProcessPriority::BelowNormal;
            TResult res = Run(cmd);
            Assert::AreEqual(0U, res.ExitCode);

            cmd.ProcessPriority = (EProcessPriority)42;
            Assert::ExpectException<TConfigurationError>([&cmd]() { Run(cmd); });
        }
    };

    TEST_CLASS(TPipelineTest)
    {
        TEST_METHOD(LongChain)
        {
            auto runChain = [](size_t numProc, const std::vector<int>& nums) {
                std::vector<TCommand> commands;
                for (size_t i = 0; i < numProc; ++i) {
                    commands.emplace_back("testApp.exe run_doubler");
                }
                {
                    std::ofstream f{"input.txt"};
                    for (int x : nums) {
                        f << x << std::endl;
                    }
                }
                commands.front().StdInput.SetFile("input.txt");
                commands.back().StdOutput.SetFile("output.txt");

                auto res = RunPipeline(commands, false);

                Assert::AreEqual(numProc, res.Results.size());
                for (size_t i = 0; i < numProc; ++i) {
                    Assert::AreEqual(0U, res.Results[i].ExitCode);
                }

                {
                    std::ifstream f{"output.txt"};
                    for (int x : nums) {
                        int ans;
                        f >> ans;
                        Assert::AreEqual(x << numProc, ans);
                    }
                }
            };

            std::vector<int> nums(1000);
            std::iota(nums.begin(), nums.end(), 0);

            for (int i = 2; i < 10; ++i) {
                runChain(i, nums);
            }
        }

        TEST_METHOD(Interactive)
        {
            std::vector<TCommand> commands;
            commands.emplace_back("testApp.exe play_number_10");
            commands.emplace_back("testApp.exe run_guesser");
            commands[0].StdError.SetFile("guessing_log.txt");
            auto result = RunPipeline(commands, true);
            Assert::AreEqual(0u, result.Results[0].ExitCode);

            std::vector<std::string> lines;
            {
                std::ifstream fin{"guessing_log.txt"};
                std::string line;
                while (std::getline(fin, line)) {
                    lines.push_back(line);
                }
            }
            Assert::AreEqual(size_t(8), lines.size());
            Assert::AreEqual(std::string{ "request: 1, response: >" }, lines[0]);
            Assert::AreEqual(std::string{ "request: 2, response: >" }, lines[1]);
            Assert::AreEqual(std::string{ "request: 4, response: >" }, lines[2]);
            Assert::AreEqual(std::string{ "request: 8, response: >" }, lines[3]);
            Assert::AreEqual(std::string{ "request: 16, response: <" }, lines[4]);
            Assert::AreEqual(std::string{ "request: 8, response: >" }, lines[5]);
            Assert::AreEqual(std::string{ "request: 12, response: <" }, lines[6]);
            Assert::AreEqual(std::string{ "request: 10, response: =" }, lines[7]);

            Assert::AreEqual(size_t(2), result.OrderOfTermination.size());
            Assert::AreEqual(size_t(1 /*player*/), result.OrderOfTermination[0]);
        }
    };
}
