#include "stdafx.h"

#include "output.h"
#include "nativeRunLib/runlib.h"

#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace NRunLib;

namespace NRunLibTests {
    TEST_CLASS(TExitCodeTest)
    {
        TEST_METHOD(Test0)
        {
            TCommand cmd;
            cmd.IsTrustedProcess = true;
            cmd.CommandLine = "testApp.exe return_0";
            auto res = Run(cmd);
            Assert::AreEqual(res.Outcome, TResult::EOutcome::Ok);
            Assert::AreEqual(res.ExitCode, 0);
        }

        TEST_METHOD(Test42)
        {
            TCommand cmd;
            cmd.IsTrustedProcess = true;
            cmd.CommandLine = "testApp.exe return_42";
            auto res = Run(cmd);
            Assert::AreEqual(res.Outcome, TResult::EOutcome::RuntimeError);
            Assert::AreEqual(res.ExitCode, 42);
        }
    };

    TEST_CLASS(TRuntimeErrorTest)
    {
        TEST_METHOD(TestException)
        {
            TCommand cmd;
            cmd.IsTrustedProcess = true;
            cmd.CommandLine = "testApp.exe exception";
            auto res = Run(cmd);
            Assert::AreNotEqual(0, res.ExitCode);
        }
    };
}
