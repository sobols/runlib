#include "stdafx.h"

#include <nativeRunLib/common.h>

#include "CppUnitTest.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace NCommonTests {
    TEST_CLASS(TEncodings)
    {
        TEST_METHOD(Utf16ToUtf8)
        {
            Assert::AreEqual(std::string{"hello"}, WideCharToStdString(L"hello"));
            Assert::AreEqual(std::string{""}, WideCharToStdString(L""));

            Assert::AreEqual(std::string{"he"}, WideCharToStdString(std::wstring_view{L"hello", 2}));
            Assert::AreEqual(std::string{"h"}, WideCharToStdString(std::wstring_view{L"hello", 1}));
            Assert::AreEqual(std::string{""}, WideCharToStdString(std::wstring_view{L"hello", 0}));
        }

        TEST_METHOD(Utf8ToUtf16)
        {
            Assert::AreEqual(std::wstring{L"hello"}, StdStringToStdWstring("hello"));
            Assert::AreEqual(std::wstring{L""}, StdStringToStdWstring(""));
        }
    };

    TEST_CLASS(TErrors)
    {
        TEST_METHOD(LastErrorText)
        {
            Assert::AreEqual(std::string{"Access is denied."}, GetLastErrorMessage(5));
            Assert::AreEqual(std::string{""}, GetLastErrorMessage(100500));
        }
    };
}
