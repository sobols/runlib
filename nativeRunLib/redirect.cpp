#include "redirect.h"

#include "attribute_list.h"
#include "common.h"

#include <Windows.h>

namespace NRunLib2 {
    namespace {
        HANDLE GetStdHandleSafe(DWORD nStdHandle) {
            HANDLE h = ::GetStdHandle(nStdHandle);

            // If the function fails, the return value is INVALID_HANDLE_VALUE
            if (h == INVALID_HANDLE_VALUE) {
                throw TWindowsError{"GetStdHandle"};
            }

            // If an application does not have associated standard handles,
            // such as a service running on an interactive desktop, the return value is NULL
            return h;
        }

        HANDLE GetOrUseStd(const TScopedHandle& h, DWORD nStdHandle) {
            if (*h == INVALID_HANDLE_VALUE) {
                return GetStdHandleSafe(nStdHandle);
            }
            return *h;
        }

        SECURITY_ATTRIBUTES MakeSecurityAttributes() {
            SECURITY_ATTRIBUTES sa = {};
            sa.nLength = sizeof(sa);
            sa.bInheritHandle = TRUE;
            return sa;
        }

        TScopedHandle SetUpFileRedirection(LPCWSTR wFileName, bool bRead) {
            SECURITY_ATTRIBUTES sa = MakeSecurityAttributes();

            HANDLE h = ::CreateFileW(
                wFileName,
                bRead ? GENERIC_READ : GENERIC_WRITE,
                FILE_SHARE_READ | FILE_SHARE_WRITE,
                &sa,
                bRead ? OPEN_EXISTING : CREATE_ALWAYS,
                FILE_FLAG_SEQUENTIAL_SCAN,
                NULL);

            if (h == INVALID_HANDLE_VALUE) {
                throw TWindowsError{"CreateFile", "SetupFileRedirection"};
            }
            return TScopedHandle{h};
        }

        bool IsReadHandle(DWORD nStdHandle) {
            return nStdHandle == STD_INPUT_HANDLE;
        }

        TScopedHandle SetUpRedirection(const TRedirectConfig& cfg, DWORD nStdHandle) {
            if (cfg.Mode == ERedirectMode::Default) {
                // Will be replaced with standard handle in GetOrUseStd()
                return TScopedHandle{INVALID_HANDLE_VALUE};
            }
            else if (cfg.Mode == ERedirectMode::Null) {
                return SetUpFileRedirection(L"NUL", IsReadHandle(nStdHandle));
            }
            else if (cfg.Mode == ERedirectMode::File) {
                if (cfg.Value.empty()) {
                    throw TConfigurationError{"filename is required"};
                }
                return SetUpFileRedirection(StdStringToStdWstring(cfg.Value).c_str(), IsReadHandle(nStdHandle));
            }
            else if (cfg.Mode == ERedirectMode::StdOutput && nStdHandle == STD_ERROR_HANDLE) {
                // pass
                return TScopedHandle{NULL};
            }
            else if (cfg.Mode == ERedirectMode::Pipe) {
                if (cfg.PipePtr == nullptr) {
                    throw TConfigurationError{"anonymous pipe is null"};
                }
                return IsReadHandle(nStdHandle) ? cfg.PipePtr->ReleaseReadHandle() : cfg.PipePtr->ReleaseWriteHandle();
            }
            else {
                throw TConfigurationError{"invalid redirection mode"};
            }
        }
    }

    TRedirectHandles::TRedirectHandles(const TRedirectConfig& input, const TRedirectConfig& output, const TRedirectConfig& error)
        : StdInput{SetUpRedirection(input, STD_INPUT_HANDLE)}
        , StdOutput{SetUpRedirection(output, STD_OUTPUT_HANDLE)}
        , StdError{SetUpRedirection(error, STD_ERROR_HANDLE)}
        , UnionOutputAndError{error.Mode == ERedirectMode::StdOutput}
    {
    }

    void TRedirectHandles::Fill(STARTUPINFO& si) const {
        si.hStdInput = GetOrUseStd(StdInput, STD_INPUT_HANDLE);
        si.hStdOutput = GetOrUseStd(StdOutput, STD_OUTPUT_HANDLE);
        si.hStdError = UnionOutputAndError ? si.hStdOutput : GetOrUseStd(StdError, STD_ERROR_HANDLE);
        si.dwFlags |= STARTF_USESTDHANDLES;
    }

    namespace {
        bool IsConsolePseudohandle(HANDLE handle) {
            return (((ULONG_PTR)(handle) & 0x3) == 0x3 && \
                GetFileType(handle) == FILE_TYPE_CHAR);
        }
    }

    void EnableStdHandleInheritance(const STARTUPINFO& si, TProcThreadAttributeList& attributeList) {
        std::vector<HANDLE> handles;

        const auto addHandle = [&handles](HANDLE h) {
            // Invalid handles (including INVALID_HANDLE_VALUE and null handles) cannot be
            // added to a PPROC_THREAD_ATTRIBUTE_LIST's PROC_THREAD_ATTRIBUTE_HANDLE_LIST.
            // If INVALID_HANDLE_VALUE appears, CreateProcess() will fail with
            // ERROR_INVALID_PARAMETER. If a null handle appears, the child process will
            // silently not inherit any handles.
            if (h == NULL || h == INVALID_HANDLE_VALUE) {
                return;
            }

            // https://github.com/python/cpython/blob/1def7754b7a41fe57efafaf5eff24cfa15353444/Lib/subprocess.py#L1317
            // https://github.com/rprichard/win32-console-docs/blob/master/README.md
            // This check can be removed once support for Windows 7 ends.
            if (IsConsolePseudohandle(h)) {
                return;
            }

            // Only unique handles
            if (std::find(handles.begin(), handles.end(), h) != handles.end()) {
                return;
            }

            handles.push_back(h);
        };

        if (si.dwFlags & STARTF_USESTDHANDLES) {
            addHandle(si.hStdInput);
            addHandle(si.hStdOutput);
            addHandle(si.hStdError);
        }

        attributeList.SetHandlesToInherit(std::move(handles));
    }

    /**
     * TAnonymousPipe
     */
    TAnonymousPipe::TAnonymousPipe() {
        SECURITY_ATTRIBUTES sa = MakeSecurityAttributes();
        HANDLE hReadPipe;
        HANDLE hWritePipe;
        if (!::CreatePipe(&hReadPipe, &hWritePipe, &sa, /*bufferSize=*/(DWORD)(16 << 20))) {
            throw TWindowsError{"CreatePipe"};
        }
        ReadPipe_ = TScopedHandle{hReadPipe};
        WritePipe_ = TScopedHandle{hWritePipe};
    }
}
