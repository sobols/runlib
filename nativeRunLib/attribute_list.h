#pragma once

#include <Windows.h>
#include <vector>

namespace NRunLib2 {
    class TProcThreadAttributeList {
    public:
        TProcThreadAttributeList();
        ~TProcThreadAttributeList();

        TProcThreadAttributeList(const TProcThreadAttributeList&) = delete;
        TProcThreadAttributeList& operator=(const TProcThreadAttributeList&) = delete;

        void SetHandlesToInherit(std::vector<HANDLE>&&);

        LPPROC_THREAD_ATTRIBUTE_LIST Get() const {
            return List_;
        }

    private:
        LPPROC_THREAD_ATTRIBUTE_LIST List_;
        std::vector<HANDLE> Handles_;
    };
}
