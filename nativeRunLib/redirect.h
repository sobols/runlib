#pragma once

#include "runlib2.h"
#include "handle.h"

#include <string>

namespace NRunLib2 {
    class TProcThreadAttributeList;

    class TRedirectHandles {
    public:
        TRedirectHandles(const TRedirectConfig& input, const TRedirectConfig& output, const TRedirectConfig& error);
        void Fill(STARTUPINFO&) const;

    private:
        TScopedHandle StdInput;
        TScopedHandle StdOutput;
        TScopedHandle StdError;
        bool UnionOutputAndError;
    };

    class TAnonymousPipe {
    public:
        TAnonymousPipe();

        HANDLE ReadHandle() {
            return *ReadPipe_;
        }
        HANDLE WriteHandle() {
            return *WritePipe_;
        }
        TScopedHandle ReleaseReadHandle() {
            return std::move(ReadPipe_);
        }
        TScopedHandle ReleaseWriteHandle() {
            return std::move(WritePipe_);
        }

    private:
        TScopedHandle ReadPipe_;
        TScopedHandle WritePipe_;
    };

    void EnableStdHandleInheritance(const STARTUPINFO&, TProcThreadAttributeList&);
}
