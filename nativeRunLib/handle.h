#pragma once

#include "exception.h"

#include <Windows.h>

#include <cassert>
#include <optional>

namespace NRunLib2 {
    class TScopedHandle {
    public:
        TScopedHandle() noexcept
            : Handle_{NULL}
        {
        }

        explicit TScopedHandle(HANDLE value) noexcept
            : Handle_{value}
        {
        }

        TScopedHandle(const TScopedHandle&) = delete;

        TScopedHandle(TScopedHandle&& other) noexcept
            : Handle_{NULL}
        {
            std::swap(Handle_, other.Handle_);
        }

        TScopedHandle& operator=(TScopedHandle&& other) noexcept {
            std::swap(Handle_, other.Handle_);
            return *this;
        }

        bool IsValid() const noexcept {
            return (Handle_ != NULL) && (Handle_ != INVALID_HANDLE_VALUE);
        }

        const HANDLE& operator*() const noexcept {
            return Handle_;
        }

        HANDLE& operator*() {
            return Handle_;
        }

        ~TScopedHandle() {
            if (IsValid()) {
                const BOOL handleClosed = ::CloseHandle(Handle_);
                assert(handleClosed);
            }
        }
    private:
        HANDLE Handle_;
    };
}
