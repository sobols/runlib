#include "common.h"

#include "exception.h"

#include <Windows.h>

#include <cassert>
#include <cmath>
#include <vector>

double BytesToMegabytes(long long bytes, int precision) {
    const double power10 = pow(10.0, precision);
    return floor(bytes * power10 / (1024 * 1024) + 0.5) / power10;
}

std::string WideCharToStdString(const wchar_t* source) {
    const size_t size = WideCharToMultiByte(CP_UTF8, 0, source, -1, NULL, 0, NULL, NULL);
    if (size == 0) {
        throw TWindowsError{"WideCharToMultiByte"};
    }
    std::string destination(size - 1, '\0');
    if (static_cast<int>(size) != WideCharToMultiByte(CP_UTF8, 0, source, -1, destination.data(), static_cast<int>(size), NULL, NULL)) {
        throw TWindowsError{"WideCharToMultiByte"};
    }
    return destination;
}

std::string WideCharToStdString(std::wstring_view source) {
    if (source.empty()) {
        return {};
    }
    const size_t size = static_cast<size_t>(WideCharToMultiByte(CP_UTF8, 0, source.data(), static_cast<int>(source.size()), NULL, 0, NULL, NULL));
    if (size == 0) {
        throw TWindowsError{"WideCharToMultiByte"};
    }
    std::string destination(size, '\0');
    if (static_cast<int>(size) != WideCharToMultiByte(CP_UTF8, 0, source.data(), static_cast<int>(source.size()), destination.data(), static_cast<int>(size), NULL, NULL)) {
        throw TWindowsError{"WideCharToMultiByte"};
    }
    return destination;
}

std::wstring StdStringToStdWstring(std::string_view source) {
    std::vector<WCHAR> destination(source.size());
    const int resultSize = MultiByteToWideChar(CP_UTF8, 0, source.data(), static_cast<int>(source.length()), destination.data(), static_cast<int>(destination.size()));
    return std::wstring(destination.begin(), destination.begin() + resultSize);
}

std::string GetLastErrorMessage(int errorCode) {
    std::string result;
    LPTSTR errorText = NULL;

    FormatMessage(
        // use system message tables to retrieve error text
        FORMAT_MESSAGE_FROM_SYSTEM
        // allocate buffer on local heap for error text
        |FORMAT_MESSAGE_ALLOCATE_BUFFER
        // Important! will fail otherwise, since we're not 
        // (and CANNOT) pass insertion parameters
        |FORMAT_MESSAGE_IGNORE_INSERTS,  
        NULL,    // unused with FORMAT_MESSAGE_FROM_SYSTEM
        errorCode,
        MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
        (LPTSTR)&errorText,  // output 
        0, // minimum size for output buffer
        NULL);

    if (!errorText) {
        return result;
    }

    // Encoding conversion
    result = WideCharToStdString(errorText);

    // cut trailing linebreak
    while (!result.empty() && (result.back() == '\r' || result.back() == '\n')) {
        result.pop_back();
    }

    LocalFree(errorText);
    return result;
}

namespace {
    HMODULE GetCurrentModule()
    { // NB: XP+ solution!
        HMODULE hModule = NULL;
        GetModuleHandleEx(
            GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS,
            (LPCTSTR)GetCurrentModule,
            &hModule);
        return hModule;
    }
}

std::string GetCurrentModuleDirectory()
{
    WCHAR path[MAX_PATH] = {0};
    GetModuleFileNameW(GetCurrentModule(), path, MAX_PATH);
    std::string fileName = WideCharToStdString(path);
    const size_t last_slash_idx = fileName.rfind('\\');
    if (std::string::npos != last_slash_idx) {
        return fileName.substr(0, last_slash_idx);
    } else {
        return fileName;
    }
}

std::string GetDosPath(const std::string& fullPath)
{
    // paths are considered to be in UTF-8

    const std::wstring fullWide = StdStringToStdWstring(fullPath);
    WCHAR buf[MAX_PATH];
    const DWORD resultLen = GetShortPathNameW(fullWide.c_str(), buf, MAX_PATH);
    return WideCharToStdString(buf);
}

std::string GetEnvVariable(const wchar_t* name)
{
    WCHAR buf[_MAX_ENV];
    DWORD len = ::GetEnvironmentVariableW(name, buf, _MAX_ENV);
    return WideCharToStdString({buf, len});
}

std::string GetTmpPath()
{
    const DWORD spaceRequired = ::GetTempPathW(0, 0);

    if (spaceRequired == 0) {
        throw TWindowsError{"GetTempPath"};
    }

    std::wstring directory;
    directory.resize(spaceRequired - 1);

    const DWORD spaceUsed = ::GetTempPathW(spaceRequired, directory.data());
    if (spaceUsed == 0) {
        throw TWindowsError{"GetTempPath"};
    }

    assert(spaceUsed < spaceRequired);
    directory.resize(spaceUsed);

    return WideCharToStdString(directory);
}

std::string GetWinPath()
{
    const DWORD spaceRequired = ::GetWindowsDirectoryW(nullptr, 0);

    if (spaceRequired == 0) {
        throw TWindowsError{"GetWindowsDirectory"};
    }

    std::wstring directory;
    directory.resize(spaceRequired - 1);

    const DWORD spaceUsed = ::GetWindowsDirectoryW(directory.data(), spaceRequired);
    if (spaceUsed + 1 != spaceRequired) {
        throw TWindowsError{"GetWindowsDirectory"};
    }

    return WideCharToStdString(directory);
}
