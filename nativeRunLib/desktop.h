#pragma once

#include "security.h"

#include <Windows.h>
#include <string>
#include <memory>

namespace NRunLib2 {
    struct TWinStaDeleter {
        using pointer = HWINSTA;
        void operator()(HWINSTA h) {
            ::CloseWindowStation(h);
        }
    };
    using TWinStaPtr = std::unique_ptr<HWINSTA, TWinStaDeleter>;

    struct TDeskDeleter {
        using pointer = HDESK;
        void operator()(HDESK h) {
            ::CloseDesktop(h);
        }
    };
    using TDeskPtr = std::unique_ptr<HDESK, TDeskDeleter>;

    class TWindowsDesktop {
    private:
        TWindowsDesktop();

    public:
        LPWSTR Name() {
            return !DesktopName_.empty() ? DesktopName_.data() : NULL;
        }

        static TWindowsDesktop& Instance() {
            static TWindowsDesktop impl{};
            return impl;
        }

    private:
        TWinStaPtr WindowStation_;
        TDeskPtr Desktop_;
        std::wstring DesktopName_;
    };
}
