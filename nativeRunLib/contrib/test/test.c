#define _UNICODE
#define UNICODE

#include <stdint.h>
#include <stdio.h>
#include <inttypes.h>
#include "w32invoke.h"

int main() {
  struct Subprocess * s = Subprocess_Create();

  // Subprocess_SetStringW(s, RUNLIB_APPLICATION_NAME, L"C:\\Program Files\\Java\\jdk1.6.0_13\\bin\\javac.exe");
  // Subprocess_SetStringW(s, RUNLIB_COMMAND_LINE, L"\"C:\\Program Files\\Java\\jdk1.6.0_13\\bin\\javac.exe\" Solution.java");
  // Subprocess_SetStringW(s, RUNLIB_APPLICATION_NAME, L"D:\\Windows\\System32\\cmd.exe");
  Subprocess_SetStringW(s, RUNLIB_COMMAND_LINE, L"D:\\Windows\\System32\\cmd.exe");
  Subprocess_SetStringW(s, RUNLIB_CURRENT_DIRECTORY, L"D:\\Temp\\bb");
  Subprocess_SetStringW(s, RUNLIB_USERNAME, L"tester1");
  Subprocess_SetStringW(s, RUNLIB_PASSWORD, L"quaiqu3E");
  //Subprocess_SetStringW(s, RUNLIB_USERNAME, L"localodmin");
  //Subprocess_SetStringW(s, RUNLIB_PASSWORD, L"Wq321654");
  Subprocess_SetInt(s, RUNLIB_TIME_LIMIT_HARD, 200000 * 1000);
  // Subprocess_SetInt(s, RUNLIB_MEMORY_LIMIT, 10240);//0000);
  // Subprocess_SetInt(s, RUNLIB_MEMORY_LIMIT_HARD, 10240);//0000);
  // Subprocess_SetBool(s, RUNLIB_CHECK_IDLENESS, 1);
  // Subprocess_SetFileRedirectW(s, Input, L"C:\\A\\contester\\runlib32\\src\\runlib32-static.o");
  // Subprocess_SetBufferOutputRedirect(s, Output);
  // Subprocess_SetBufferOutputRedirect(s, Error);
  // Subprocess_SetBool(s, RUNLIB_NO_JOB, 1);
  // Subprocess_SetBool(s, RESTRICT_UI, 1);
  if (!Subprocess_Start(s)) {
    printf("Error :(\n");
    while (Subprocess_HasError(s)) {
      const struct SubprocessErrorEntry e = Subprocess_PopError(s);
      printf("Error: %d, le: %d\n", e.error_id, e.dwLastError);
    }
    return 0;
  }
  Subprocess_Wait(s);

  const struct SubprocessResult * const sr = Subprocess_GetResult(s);

  printf("Exit code: %d\nTime: %" PRIu64 " %" PRIu64 " %" PRIu64 "\nMemory: %" PRIu64 "\nFlags: %d\n", sr->ExitCode, sr->ttUser, sr->ttKernel, sr->ttWall, sr->PeakMemory, sr->SuccessCode);
  // printf("Output: %s\n", Subprocess_GetRedirectBuffer(s, Output)->cBuffer);

  while (Subprocess_HasError(s)) {
    const struct SubprocessErrorEntry e = Subprocess_PopError(s);
    printf("Error: %d, le: %d\n", e.error_id, e.dwLastError);
  }


  return 0;
}
