#include <windows.h>
#include "inject.h"

BOOL InjectLibW(DWORD dwProcessId, LPCWSTR pszLibFile)
{
    BOOL bSuccess = FALSE; // Assume that the function fails
    HANDLE hProcess = 0;
    HANDLE hThread = 0;
    void* pszLibFileRemote = NULL;
    DWORD cch;
    DWORD cb;
    DWORD done;
    FARPROC pfnThreadRtn;
    DWORD dwExitCode;

    // Inject actions.
    {
        // Get a handle for the target process.
        hProcess = OpenProcess(
            PROCESS_QUERY_INFORMATION |   // Required by Alpha
            PROCESS_CREATE_THREAD     |   // For CreateRemoteThread
            PROCESS_VM_OPERATION      |   // For VirtualAllocEx/VirtualFreeEx
            PROCESS_VM_WRITE,             // For WriteProcessMemory
            FALSE, dwProcessId);

        if (hProcess == 0)
            goto onError;

        // Calculate the number of bytes needed for the DLL's pathname
        cch = 1 + lstrlenW(pszLibFile);
        cb  = cch * sizeof(WCHAR);

        // Allocate space in the remote process for the pathname
        pszLibFileRemote = (LPWSTR)(
            VirtualAllocEx(hProcess, NULL, cb, MEM_COMMIT, PAGE_READWRITE));

        if (pszLibFileRemote == NULL)
            goto onError;

        // Copy the DLL's pathname to the remote process's address space
        if (!WriteProcessMemory(hProcess, pszLibFileRemote,
            pszLibFile, cb, &done))
            goto onError;

        // Get the real address of LoadLibraryW in Kernel32.dll
        pfnThreadRtn =
            GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
        if (pfnThreadRtn == NULL) 
            goto onError;

        // Create a remote thread that calls LoadLibraryW(DLLPathname)
        hThread = CreateRemoteThread(hProcess, NULL, 0,
            (LPTHREAD_START_ROUTINE)pfnThreadRtn, pszLibFileRemote, 0, &done);
        if (hThread == NULL) 
            goto onError;

        // Wait for the remote thread to terminate
        if (WaitForSingleObject(hThread, INFINITE) != WAIT_OBJECT_0)
            goto onError;

        if (!GetExitCodeThread(hThread, &dwExitCode))
            goto onError;

        bSuccess = dwExitCode != 0 ? TRUE : FALSE; // Everything executed successfully
    }

    // Close all the handles.
onError:
    {
        if (pszLibFileRemote != NULL)
            VirtualFreeEx(hProcess, pszLibFileRemote, 0, MEM_RELEASE);

        if (hThread  != NULL)
            CloseHandle(hThread);

        if (hProcess != NULL)
            CloseHandle(hProcess);
    }

    return bSuccess;
}
