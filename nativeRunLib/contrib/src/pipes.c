#define _WIN32_WINNT 0x510
#include <windows.h>
#include <stdint.h>
//#include <stdbool.h>
#include "win32-gcc.h"
#include "winmisc.h"
#include "w32invoke.h"
#include "pipes.h"

static const int PIPE_BUFFER_PREALLOC = 16384;
static const int PIPE_BUFFER_INCREMENT = 65536;
static const int PIPE_BUFFER_OVERFLOW = 1024*1024;

struct PipeBuffer {
    HANDLE hPipe;
    bool bOverflow;
    struct SubprocessPipeBuffer sppb;
};

struct RedirectedPipe {
    struct PipeBuffer pbBuffer;
    HANDLE hThread;
};

enum REDIRECT_TYPE {
    REDIRECT_PIPE,
    REDIRECT_FILE
};

struct RedirectParameters {
    bool IsRead;
    enum REDIRECT_TYPE Type;
    HANDLE hPipe;
    struct RedirectedPipe rpPipe;
};

struct RedirectParameters * RedirectParameters_Alloc() {
    struct RedirectParameters * const result = malloc(
        sizeof(struct RedirectParameters));

    ZeroMemory(result, sizeof(struct RedirectParameters));
    result->hPipe = result->rpPipe.pbBuffer.hPipe =
        result->rpPipe.hThread = INVALID_HANDLE_VALUE;

    return result;
}

static HANDLE SetupFileRedirection(const WCHAR * const wFileName, const bool bRead) {
    SECURITY_ATTRIBUTES sa = { 0 };

    sa.nLength = sizeof(sa);
    sa.bInheritHandle = 1;

    return CreateFileW(
        wFileName,
        bRead ? GENERIC_READ : GENERIC_WRITE,
        FILE_SHARE_READ | FILE_SHARE_WRITE,
        &sa,
        bRead ? OPEN_EXISTING : CREATE_ALWAYS,
        FILE_FLAG_SEQUENTIAL_SCAN,
        NULL);
}

bool RedirectParameters_SetupFileRedirection(
struct RedirectParameters * const self,
    const WCHAR * const wFileName,
    const bool bRead) {
        if (wFileName) {
            const HANDLE hFile = SetupFileRedirection(wFileName, bRead);
            if (likely(hFile != INVALID_HANDLE_VALUE)) {
                self->hPipe = hFile;
                self->Type = REDIRECT_FILE;
                self->IsRead = bRead;
                return true;
            }
        }
        return false;
}

bool RedirectParameters_SetupBufferRedirection(
struct RedirectParameters * const self, const bool bRead) {
    SECURITY_ATTRIBUTES sa = { 0 };
    HANDLE * const phProcessEndpoint = &self->hPipe;
    HANDLE * const phManagerEndpoint = &self->rpPipe.pbBuffer.hPipe;
    sa.nLength = sizeof(sa);
    sa.bInheritHandle = 1;

    if (!CreatePipe(
        bRead ? phProcessEndpoint : phManagerEndpoint,
        bRead ? phManagerEndpoint : phProcessEndpoint,
        &sa, 0))
        return false;

    if (!SetHandleInformation(
        self->rpPipe.pbBuffer.hPipe, HANDLE_FLAG_INHERIT, 0)) {
            ClearHandle(phProcessEndpoint);
            ClearHandle(phManagerEndpoint);

            return false;
    }

    self->Type = REDIRECT_PIPE;
    self->IsRead = bRead;
    return true;
}

enum PIPE_READER_EXIT_CODES {
    PIPE_READER_OK,
    PIPE_READER_OVERFLOW,
    PIPE_READER_REALLOC_FAILED,
    PIPE_READER_READ_FAILED,
    PIPE_READER_PREALLOC_FAILED,
};

static DWORD WINAPI PipeReaderThread(LPVOID lppb) {
    DWORD AllocatedSize;
    struct PipeBuffer * const pb = (struct PipeBuffer *) lppb;
    enum PIPE_READER_EXIT_CODES eExitCode = PIPE_READER_OK;
    char * cNewBuffer;
    DWORD dwRead = 0;
    bool bReadFile;

    if (!(pb->sppb.cBuffer = malloc(PIPE_BUFFER_PREALLOC))) {
        ClearHandle(&pb->hPipe);
        return PIPE_READER_PREALLOC_FAILED;
    }
    AllocatedSize = PIPE_BUFFER_PREALLOC;
    pb->sppb.Size = 0;

    for(;;) {
        if (AllocatedSize < (pb->sppb.Size + PIPE_BUFFER_PREALLOC)) {
            if (pb->sppb.Size > PIPE_BUFFER_OVERFLOW) {
                pb->bOverflow = true;
                eExitCode = PIPE_READER_OVERFLOW;
                break;
            }

            cNewBuffer = realloc(
                pb->sppb.cBuffer, AllocatedSize + PIPE_BUFFER_INCREMENT);
            if (!cNewBuffer) {
                eExitCode = PIPE_READER_REALLOC_FAILED;
                break;
            }
            pb->sppb.cBuffer = cNewBuffer;
            AllocatedSize += PIPE_BUFFER_INCREMENT;
        }

        dwRead = 0;
        bReadFile = ReadFile(
            pb->hPipe, &(pb->sppb.cBuffer[pb->sppb.Size]), AllocatedSize - pb->sppb.Size,
            &dwRead, NULL);
        pb->sppb.Size += dwRead;

        if (!bReadFile) {
            eExitCode = PIPE_READER_READ_FAILED;
            break;
        }
    }

    ClearHandle(&pb->hPipe);

    return eExitCode;
}

static const bool __const__ IsThisAPipe(const struct RedirectParameters * const rp) {
    return (rp && (rp->Type == REDIRECT_PIPE));
}

bool RedirectParameters_CreateThread(struct RedirectParameters * const rp) {
    if (IsThisAPipe(rp)
        && (rp->rpPipe.pbBuffer.hPipe != INVALID_HANDLE_VALUE)
        && (rp->rpPipe.hThread == INVALID_HANDLE_VALUE))
    {
        const HANDLE hThread = CreateThread(
            NULL, 0, &PipeReaderThread, &rp->rpPipe.pbBuffer, 0, NULL);
        if (unlikely(hThread == INVALID_HANDLE_VALUE))
            return false;
        rp->rpPipe.hThread = hThread;
    }
    return true;
}

void RedirectParameters_CloseProcessEnd(struct RedirectParameters * const rp) {
    if (rp)
        ClearHandle(&rp->hPipe);
}

int RedirectParameters_Wait(struct RedirectParameters * const rp) {
    int iReturnCode = 1;
    // TODO: parse thread exit code here
    if (IsThisAPipe(rp) && (rp->rpPipe.hThread != INVALID_HANDLE_VALUE)) {
        iReturnCode = (WAIT_TIMEOUT != WaitForSingleObject(rp->rpPipe.hThread, 10000));
        ClearHandle(&rp->rpPipe.hThread);
    }

    return iReturnCode;
}

bool RedirectParameters_KillThread(struct RedirectParameters * const rp) {
    if (IsThisAPipe(rp) && (rp->rpPipe.hThread != INVALID_HANDLE_VALUE)) {
        // TODO: do something useful here
    }
    return true;
}

static void RedirectParameters_Close(struct RedirectParameters * const rp) {
    if (!rp)
        return;

    ClearHandle(&rp->rpPipe.pbBuffer.hPipe);
    if (rp->rpPipe.pbBuffer.sppb.cBuffer)
        free(rp->rpPipe.pbBuffer.sppb.cBuffer);
    free(rp);
}

void RedirectParameters_Clear(struct RedirectParameters ** const rp) {
    RedirectParameters_Close(*rp);
    *rp = NULL;
}

const bool RedirectParameters_IsOverflow(const struct RedirectParameters * const rp) {
    return rp && rp->rpPipe.pbBuffer.bOverflow;
}

const HANDLE RedirectParameters_GetPipeHandle(const struct RedirectParameters * const rp) {
    return rp ? rp->hPipe : INVALID_HANDLE_VALUE;
}

const struct SubprocessPipeBuffer * const RedirectParameters_GetPipeBuffer(
    const struct RedirectParameters * const self) {
        return self ? &(self->rpPipe.pbBuffer.sppb) : NULL;
}
