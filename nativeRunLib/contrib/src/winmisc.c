#define _WIN32_WINNT 0x510
#include <windows.h>
#include "win32-gcc.h"
#include "winmisc.h"

void ClearHandle(HANDLE * const hObj) {
    if (unlikely(hObj != INVALID_HANDLE_VALUE)) {
        CloseHandle(*hObj);
        *hObj = INVALID_HANDLE_VALUE;
    }
}
