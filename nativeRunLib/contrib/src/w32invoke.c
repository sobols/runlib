// Copyright (c) 1996-2007 Paul P. Komkoff Jr
// For license information, read file LICENSE in project top directory
//
// Native interface to Win32 process API

#define _WIN32_WINNT 0x510
#define _GNU_SOURCE
#define _UNICODE

#include <windows.h>
#include <psapi.h>
#include <userenv.h>
#include <winsafer.h>

#include <wchar.h>

#include <stdio.h>
#include <stdint.h>
#include <string.h>
//#include <stdbool.h>
#include "mystdbool.h"

#include <w32invoke.h>

#include "winmisc.h"
#include "pipes.h"
#include "inject.h"

static const int DEFAULT_TIME_QUANTUM = 50;
static const int QUANTA_FOR_IDLENESS = 40;

static const wchar_t * const wDot = L".";

struct Subprocess {
    HANDLE hJob, hProcess, bhThread, hUser,
        hProfile, hWindowStation, hDesktop, hThread;

    WCHAR
        *wApplicationName,
        *wCommandLine,
        *wCurrentDirectory,
        *wEnvironment,

        *wUsername,
        *wPassword,
        *wDomain,

        *wInjectDll;

    bool NoJob;
    bool RestrictUI;
    bool DropRights;
    bool AccountWorkingSet;
    unsigned int ProcessLimit;

    struct SubprocessErrorEntry ErrorEntries[32];
    unsigned int Errors;
    CRITICAL_SECTION csError;

    bool CheckIdleness;
    uint64_t TimeLimit;
    uint64_t HardTimeLimit;
    uint64_t MemoryLimit;
    uint64_t HardMemoryLimit;
    uint64_t TimeQuantum;

    struct SubprocessResult srResult;
    struct RedirectParameters * rp[REDIRECT_LAST];

    SubprocessCbFunc cb;
    void * cbarg;

    void* (*mallocfunc)(size_t);
    void* (*reallocfunc)(void*, size_t);
    void (*freefunc)(void*);

    int IdleQuanta;
    DWORD ProcessErrorMode;
};

#define for_each_redirect(x) for(x = 0; x < REDIRECT_LAST; x++)

// ---

static void ClearStringW(WCHAR **wDst) {
    if (*wDst)
        free(*wDst);
    *wDst = NULL;
}

static void ExecuteParameters_Clear(struct Subprocess * const self) {
    ClearStringW(&self->wApplicationName);
    ClearStringW(&self->wCommandLine);
    ClearStringW(&self->wCurrentDirectory);
    ClearStringW(&self->wEnvironment);

    ClearStringW(&self->wUsername);
    ClearStringW(&self->wPassword);
    ClearStringW(&self->wDomain);
}

static void Redirects_CloseClear(struct Subprocess * const self) {
    enum REDIRECTION_KEY x;
    for_each_redirect(x)
        RedirectParameters_Clear(&self->rp[x]);
}

static void Redirects_CloseProcessEnd(struct Subprocess * const self) {
    enum REDIRECTION_KEY x;
    for_each_redirect(x)
        RedirectParameters_CloseProcessEnd(self->rp[x]);
}

static void SubprocessWipe(struct Subprocess * const self) {
    ZeroMemory(self, sizeof(*self));

    self->hJob =
        self->hProcess =
        self->bhThread =
        self->hUser =
        self->hProfile =
        self->hWindowStation =
        self->hDesktop =
        INVALID_HANDLE_VALUE;

    self->TimeQuantum = DEFAULT_TIME_QUANTUM;
    self->IdleQuanta = 0;
    InitializeCriticalSection(&self->csError);

    self->ProcessErrorMode = SetErrorMode(SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX);
    SetErrorMode(self->ProcessErrorMode | SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX);
}

struct Subprocess * USED Subprocess_CreateEx(
    void* (*mallocfunc)(size_t),
    void* (*reallocfunc)(void*, size_t),
    void (*freefunc)(void*))
{
    struct Subprocess * const result = mallocfunc ? mallocfunc(sizeof(struct Subprocess)) : malloc(sizeof(struct Subprocess));
    if (result) {
        SubprocessWipe(result);
        result->mallocfunc = mallocfunc;
        result->reallocfunc = reallocfunc;
        result->freefunc = freefunc;
    }

    return result;
}

struct Subprocess * USED Subprocess_Create() {
    return Subprocess_CreateEx(NULL, NULL, NULL);
}

void USED Subprocess_Destroy(struct Subprocess * self) {
    DeleteCriticalSection(&self->csError);
    Redirects_CloseClear(self);

    SetErrorMode(self->ProcessErrorMode);

    if (self->freefunc)
        self->freefunc(self);
    else
        free(self);
}

bool Subprocess_PushError(struct Subprocess * const self, enum SubprocessErrorID id) {
    bool bResult;
    const DWORD le = GetLastError();
    EnterCriticalSection(&self->csError);
    bResult = (self->Errors < 31);
    if (bResult) {
        self->ErrorEntries[self->Errors].error_id = id;
        self->ErrorEntries[self->Errors].dwLastError = le;
        self->Errors++;
    }
    LeaveCriticalSection(&self->csError);
    return bResult;
}

struct SubprocessErrorEntry USED Subprocess_PopError(struct Subprocess * const self) {
    struct SubprocessErrorEntry result = { 0 };

    EnterCriticalSection(&self->csError);
    if (self->Errors) {
        result = self->ErrorEntries[self->Errors - 1];
        self->Errors--;
    }
    LeaveCriticalSection(&self->csError);
    return result;
}

int USED Subprocess_HasError(const struct Subprocess * const self) {
    return self->Errors;
}

static bool SetStringWB(WCHAR **wDst, const void * const Value) {
    if (!Value)
        return false;

    ClearStringW(wDst);
    *wDst = Value;
    return true;
}

static bool SetBuffer(WCHAR **wDst, const void * const Value, const int Length, const UINT CodePage) {
    size_t resultLen = 0;
    bool result = false;
    const char * const cValue = Value;
    const size_t size = MultiByteToWideChar(CodePage, 0, cValue, Length, NULL, 0);
    WCHAR * const wBuf = malloc((size + 1) * sizeof(WCHAR));

    if (!wBuf)
        return false;

    resultLen = MultiByteToWideChar(CodePage, 0, cValue, Length, wBuf, size);
    wBuf[resultLen] = 0;

    result = SetStringWB(wDst, wBuf);

    if (!result)
        free(wBuf);

    return result;
}

static bool SetStringA(WCHAR **wDst, const void * const Value) {
    return SetBuffer(wDst, Value, -1, CP_ACP);
}

static bool SetStringW(WCHAR **wDst, const void * const Value) {
    return SetStringWB(wDst, _wcsdup(Value));
}

static bool SetBufferUTF8(WCHAR **wDst, const void * const Value, const int Length) {
    return SetBuffer(wDst, Value, Length, CP_UTF8);
}

static bool SetStringUTF8(WCHAR **wDst, const void * const Value) {
    return SetBufferUTF8(wDst, Value, -1);
}

static WCHAR ** __const__ GetStringField(
    const struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param) {
        switch (param) {
        case RUNLIB_APPLICATION_NAME:
            return &(self->wApplicationName);
        case RUNLIB_COMMAND_LINE:
            return &(self->wCommandLine);
        case RUNLIB_CURRENT_DIRECTORY:
            return &(self->wCurrentDirectory);
        case RUNLIB_ENVIRONMENT:
            return &(self->wEnvironment);
        case RUNLIB_USERNAME:
            return &(self->wUsername);
        case RUNLIB_PASSWORD:
            return &(self->wPassword);
        case RUNLIB_DOMAIN:
            return &(self->wDomain);
        case RUNLIB_INJECT_DLL:
            return &(self->wInjectDll);
        default:
            return NULL;
        }
}

int USED Subprocess_SetStringA(
struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param,
    const char * const cValue) {
        return SetStringA(GetStringField(self, param), cValue);
}

int USED Subprocess_SetStringW(
struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param,
    const wchar_t * const wValue) {

        return SetStringW(GetStringField(self, param), wValue);
}

int USED Subprocess_SetStringWB(
struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param,
    const wchar_t * const wValue) {

        return SetStringWB(GetStringField(self, param), wValue);
}

int USED Subprocess_SetStringUTF8(
struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param,
    const char * const cValue) {

        return SetStringUTF8(GetStringField(self, param), cValue);
}

int USED Subprocess_SetBufferUTF8(
struct Subprocess * const self,
    const enum SUBPROCESS_PARAM param,
    const char * const cValue,
    const int iLength) {

        return SetBufferUTF8(GetStringField(self, param), cValue, iLength);
}


int USED Subprocess_SetInt(
struct Subprocess * const self, const enum SUBPROCESS_PARAM param, const uint64_t iValue) {
    switch (param) {
    case RUNLIB_TIME_LIMIT:
        self->TimeLimit = iValue;
        break;
    case RUNLIB_TIME_LIMIT_HARD:
        self->HardTimeLimit = iValue;
        break;
    case RUNLIB_MEMORY_LIMIT:
        self->MemoryLimit = iValue;
        break;
    case RUNLIB_MEMORY_LIMIT_HARD:
        self->HardMemoryLimit = iValue;
        break;
    case RUNLIB_PROCESS_LIMIT:
        self->ProcessLimit = iValue;
        break;
    default:
        return 0;
    }

    return 1;
}

int USED Subprocess_SetBool(
struct Subprocess * const self, const enum SUBPROCESS_PARAM param, const int bValue) {
    switch (param) {
#define B(x, v) case x: \
    self->v = bValue; \
    break;
        B(RUNLIB_CHECK_IDLENESS, CheckIdleness)
        B(RUNLIB_RESTRICT_UI, RestrictUI)
        B(RUNLIB_NO_JOB, NoJob)
        B(RUNLIB_DROP_RIGHTS, DropRights)
        B(RUNLIB_ACCOUNT_WORKING_SET, AccountWorkingSet)
#undef B
default:
    return 0;
    }

    return 1;
}

int USED Subprocess_SetCallback(
struct Subprocess * const self, const SubprocessCbFunc cb, void * const cbarg) {
    self->cb = cb;
    self->cbarg = cbarg;

    return 1;
}

int USED Subprocess_SetFileRedirectW(struct Subprocess * const self, const enum REDIRECTION_KEY key, const wchar_t * const wFileName) {
    struct RedirectParameters * const rp = RedirectParameters_Alloc();

    RedirectParameters_SetupFileRedirection(rp, wFileName, key == Input ? true : false);
    self->rp[key] = rp;

    return 1;
}

static int SetFileRedirectB(struct Subprocess * const self, const enum REDIRECTION_KEY key, const void * const Value, const int Length, const UINT CodePage) {
    size_t resultLen = 0;
    int result = 0;
    const char * const cValue = Value;
    const size_t size = MultiByteToWideChar(CodePage, 0, cValue, Length, NULL, 0);
    WCHAR * const wBuf = malloc((size + 1) * sizeof(WCHAR));

    if (!wBuf)
        return 0;

    resultLen = MultiByteToWideChar(CodePage, 0, cValue, Length, wBuf, size);
    wBuf[resultLen] = 0;

    result = Subprocess_SetFileRedirectW(self, key, wBuf);

    if (!result)
        free(wBuf);

    return result;
}

int USED Subprocess_SetFileRedirectA(struct Subprocess * const self, const enum REDIRECTION_KEY key, const char * const cFileName) {
    return SetFileRedirectB(self, key, cFileName, -1, CP_ACP);
}
int USED Subprocess_SetFileRedirectUTF8(struct Subprocess * const self, const enum REDIRECTION_KEY key, const char * const cFileName) {
    return SetFileRedirectB(self, key, cFileName, -1, CP_UTF8);
}

int USED Subprocess_SetFileRedirectBufferUTF8(struct Subprocess * const self, const enum REDIRECTION_KEY key, const void * const Value, const int Length) {
    return SetFileRedirectB(self, key, Value, Length, CP_UTF8);
}


int USED Subprocess_SetBufferOutputRedirect(struct Subprocess * const self, const enum REDIRECTION_KEY key) {
    struct RedirectParameters * const rp = RedirectParameters_Alloc();

    RedirectParameters_SetupBufferRedirection(rp, 0);

    self->rp[key] = rp;

    return 1;
}

static const HANDLE __const__ GetStdHandleByKey(const enum REDIRECTION_KEY key) {
    static const DWORD KeyToStdMap[] = { STD_INPUT_HANDLE, STD_OUTPUT_HANDLE, STD_ERROR_HANDLE };

    return GetStdHandle(KeyToStdMap[key]);
}

static HANDLE * const __const__ GetHandleByKey(STARTUPINFOW * const si, const enum REDIRECTION_KEY key) {
    switch (key) {
    case Input:
        return &si->hStdInput;
    case Output:
        return &si->hStdOutput;
    case Error:
        return &si->hStdError;
    default:
        return NULL;
    }
}

static HANDLE LoadProfile(const HANDLE hUser, wchar_t * const wUsername) {
    /*
    PROFILEINFOW pInfo = { 0 };
    pInfo.dwSize = sizeof(pInfo);
    pInfo.dwFlags = PI_NOUI;
    pInfo.lpUserName = wUsername;

    if (LoadUserProfileW(hUser, &pInfo))
        return pInfo.hProfile;
    else
        return INVALID_HANDLE_VALUE;
    */
}

static bool CreateDesktopz(struct Subprocess * const self, wchar_t * const wOutBuf) {
    const HWINSTA hOriginalWindowStation = GetProcessWindowStation();
    const HDESK hOriginalDesktop = GetThreadDesktop(GetCurrentThreadId());
    HWINSTA hWindowStation;
    HDESK hDesktop;
    wchar_t wWindowStation[128] = {0};
    wchar_t wDesktop[128] = {0};
    bool bResult = false;
    DWORD windowStationNameLength = 0;
    FILE* f = 0;

    wsprintfW(wWindowStation, L"contester%I32u", (uint32_t) self); //GetCurrentProcessId());
    //wWindowStation is left empty. MSDN: Only members of the Administrators group are allowed to specify a name.
    wsprintfW(wDesktop, L"contester");

    hWindowStation = CreateWindowStationW(
        wWindowStation,
        0, MAXIMUM_ALLOWED, NULL);

    if (hWindowStation) {
        // get the name of window station
        if (GetUserObjectInformationW(hWindowStation, UOI_NAME, wWindowStation, sizeof(wWindowStation), &windowStationNameLength)) {
            if (SetProcessWindowStation(hWindowStation)) {
                hDesktop = CreateDesktopW(
                    wDesktop,
                    NULL, NULL, 0, GENERIC_ALL, NULL);

                if (hDesktop) {
                    wsprintfW(wOutBuf, L"%s\\%s", wWindowStation, wDesktop);

                    SetThreadDesktop(hOriginalDesktop);
                    self->hWindowStation = hWindowStation;
                    self->hDesktop = hDesktop;
                    bResult = true;
                } else
                    Subprocess_PushError(self, EID_CREATE_DESKTOP);

                SetProcessWindowStation(hOriginalWindowStation);
            } else {
                Subprocess_PushError(self, EID_SET_PROCESS_WINDOW_STATION);
            }
            if (!bResult)
                CloseWindowStation(hWindowStation);
        } else {
            Subprocess_PushError(self, EID_GET_WINDOW_STATION_NAME);
        }
    } else
        Subprocess_PushError(self, EID_CREATE_WINDOW_STATION);
    return bResult;
}


static bool LaunchModeCurUserDropRights(struct Subprocess * const self,
                                        STARTUPINFOW * const siStartupInfo,
                                        SAFER_LEVEL_HANDLE hAuthzLevel,
                                        PROCESS_INFORMATION * const piProcessInfo)
{
    //  Generate the restricted token we will use.
    bool bResult = false;
    HANDLE hToken = NULL;
    if (SaferComputeTokenFromLevel(
        hAuthzLevel,    // SAFER Level handle
        NULL,           // NULL is current thread token.
        &hToken,        // Target token
        0,              // No flags
        NULL))          // Reserved
    {
        // Spin up the new process
        if (CreateProcessAsUserW( 
            hToken,
            self->wApplicationName, self->wCommandLine,
            NULL, NULL,
            TRUE,
            CREATE_NEW_PROCESS_GROUP | CREATE_NEW_CONSOLE | CREATE_SUSPENDED | CREATE_UNICODE_ENVIRONMENT | CREATE_BREAKAWAY_FROM_JOB,
            self->wEnvironment,
            self->wCurrentDirectory,
            siStartupInfo, piProcessInfo))
        {
            bResult = true;
        } else {
            Subprocess_PushError(self, EID_CREATE_PROCESS_AS_USER);
        }

        CloseHandle(hToken);
    } else {
        Subprocess_PushError(self, EID_SAFER_COMPUTE_TOKEN_FROM_LEVEL);
    }
    return bResult;
}


static bool LaunchModeAnyUser(struct Subprocess * const self,
                              STARTUPINFOW * const siStartupInfo,
                              SAFER_LEVEL_HANDLE hAuthzLevel,
                              PROCESS_INFORMATION * const piProcessInfo)
{
    HANDLE hTokenForCreateProcess = NULL;
    wchar_t * const wDomain = self->wDomain ? self->wDomain : wDot;
    if (!self->NoJob) {
        if (LogonUserW(
            self->wUsername,
            wDomain,
            self->wPassword,
            LOGON32_LOGON_BATCH,
            LOGON32_PROVIDER_DEFAULT,
            &self->hUser))
        {
            /*if ((self->hProfile = LoadProfile(self->hUser, self->wUsername)) != INVALID_HANDLE_VALUE)*/ {
                wchar_t wBuf[512] = { 0 };
                wcscpy(wBuf, L"");

                if (true) { //(CreateDesktopz(self, wBuf)) {
                    bool bResult = true;
                    siStartupInfo->lpDesktop = wBuf;

                    hTokenForCreateProcess = self->hUser;
                    if (self->DropRights) {
                        if (!SaferComputeTokenFromLevel(
                            hAuthzLevel,                // SAFER Level handle
                            self->hUser,
                            &hTokenForCreateProcess,    // Target token
                            0,                          // No flags
                            NULL))                      // Reserved
                        {
                            bResult = false;
                            Subprocess_PushError(self, EID_SAFER_COMPUTE_TOKEN_FROM_LEVEL);
                        }
                    }

                    if (bResult) {
                        bResult = CreateProcessAsUserW(
                            hTokenForCreateProcess,
                            self->wApplicationName,
                            self->wCommandLine,
                            NULL,
                            NULL,
                            TRUE,
                            CREATE_NEW_PROCESS_GROUP | CREATE_NEW_CONSOLE | CREATE_SUSPENDED | CREATE_UNICODE_ENVIRONMENT | CREATE_BREAKAWAY_FROM_JOB,
                            self->wEnvironment,
                            self->wCurrentDirectory,
                            siStartupInfo,
                            piProcessInfo);
                        
                        if (self->DropRights) {
                            CloseHandle(hTokenForCreateProcess);
                        }

                        if (!bResult) {
                            Subprocess_PushError(self, EID_CREATE_PROCESS_AS_USER);
                        } else {
                            return true;
                        }
                    }
                }
                //UnloadUserProfile(self->hUser, self->hProfile);
                //ClearHandle(&self->hProfile);
            } /*else {
                Subprocess_PushError(self, EID_LOAD_USER_PROFILE);
                ClearHandle(&self->hUser);
            }*/
        } else {
            Subprocess_PushError(self, EID_LOGON_USER);
        }
    }
    return false;
    /*

    // Job assignment does not work with CreateProcessWithLogonW
    // We do not want running without job at all.

    siStartupInfo.lpDesktop = L"";
    return !!CreateProcessWithLogonW(
        self->wUsername,
        self->wDomain ? self->wDomain : L".",
        self->wPassword,
        LOGON_WITH_PROFILE,
        self->wApplicationName,
        self->wCommandLine,
        CREATE_SUSPENDED | CREATE_UNICODE_ENVIRONMENT,
        self->wEnvironment,
        self->wCurrentDirectory,
        &siStartupInfo,
        piProcessInfo);
    */
}

static bool LaunchModeSimple(struct Subprocess * const self,
                            STARTUPINFOW * const siStartupInfo,
                            PROCESS_INFORMATION * const piProcessInfo)
{
    return !!CreateProcessW(
        self->wApplicationName,
        self->wCommandLine,
        (LPSECURITY_ATTRIBUTES) NULL,
        (LPSECURITY_ATTRIBUTES) NULL,
        TRUE,
        CREATE_NEW_PROCESS_GROUP | CREATE_NEW_CONSOLE | CREATE_SUSPENDED | CREATE_UNICODE_ENVIRONMENT | CREATE_BREAKAWAY_FROM_JOB,
        self->wEnvironment,
        self->wCurrentDirectory,
        siStartupInfo,
        piProcessInfo);
}

static bool LaunchProcess(struct Subprocess * const self, PROCESS_INFORMATION * const piProcessInfo) {
    enum REDIRECTION_KEY x;
    STARTUPINFOW siStartupInfo = { 0 };
    bool bResult = FALSE;
    
    // DropRights
    SAFER_LEVEL_HANDLE hAuthzLevel = NULL;

    siStartupInfo.cb = sizeof(siStartupInfo);
    siStartupInfo.dwFlags = STARTF_FORCEOFFFEEDBACK | STARTF_USESHOWWINDOW;
    siStartupInfo.wShowWindow = SW_HIDE; // SW_SHOWMINNOACTIVE
    siStartupInfo.hStdInput =
        siStartupInfo.hStdOutput =
        siStartupInfo.hStdError = INVALID_HANDLE_VALUE;

    {
        bool bRedirect = false;
        for_each_redirect(x) {
            const HANDLE h = RedirectParameters_GetPipeHandle(self->rp[x]);

            *GetHandleByKey(&siStartupInfo, x) = (h != INVALID_HANDLE_VALUE) ? h : GetStdHandleByKey(x);
            bRedirect |= (h != INVALID_HANDLE_VALUE);
        }

        if (bRedirect)
            siStartupInfo.dwFlags |= STARTF_USESTDHANDLES;
    }

    if (self->DropRights) {
        // see http://msdn.microsoft.com/en-us/library/ms972827.aspx
        DWORD hSaferLevel = SAFER_LEVELID_CONSTRAINED;
        
        if (!SaferCreateLevel(SAFER_SCOPEID_USER,
                              hSaferLevel,
                              0, 
                              &hAuthzLevel, NULL))
        {    
            Subprocess_PushError(self, EID_SAFER_CREATE_LEVEL);
            return false;
        }
    }

    if (!self->wUsername && self->DropRights) {
        bResult = LaunchModeCurUserDropRights(self, &siStartupInfo, hAuthzLevel, piProcessInfo);
    } else if (self->wUsername) {
        bResult = LaunchModeAnyUser(self, &siStartupInfo, hAuthzLevel, piProcessInfo);
    } else {
        bResult = LaunchModeSimple(self, &siStartupInfo, piProcessInfo);
    }

    if (self->DropRights) {
        SaferCloseLevel(hAuthzLevel);
    }
    return bResult;
}

static HANDLE CreateSubprocessJob(struct Subprocess * const self) {
    const HANDLE hJob = CreateJobObject(NULL, NULL);

    if (!hJob)
        return INVALID_HANDLE_VALUE;

    {
        JOBOBJECT_EXTENDED_LIMIT_INFORMATION eInfo = { 0 };

        if (self->TimeLimit) {
            eInfo.BasicLimitInformation.PerProcessUserTimeLimit.QuadPart =
                eInfo.BasicLimitInformation.PerJobUserTimeLimit.QuadPart = self->TimeLimit * 10;

            eInfo.BasicLimitInformation.LimitFlags |=
                JOB_OBJECT_LIMIT_JOB_TIME |
                JOB_OBJECT_LIMIT_PROCESS_TIME;
        }

        if (self->ProcessLimit) {
            eInfo.BasicLimitInformation.ActiveProcessLimit = self->ProcessLimit;
            eInfo.BasicLimitInformation.LimitFlags |= JOB_OBJECT_LIMIT_ACTIVE_PROCESS;
        }

        if (self->HardMemoryLimit) {
            eInfo.ProcessMemoryLimit =
                eInfo.JobMemoryLimit =
                eInfo.BasicLimitInformation.MinimumWorkingSetSize =
                eInfo.BasicLimitInformation.MaximumWorkingSetSize =
                self->HardMemoryLimit;

            eInfo.BasicLimitInformation.LimitFlags |=
                JOB_OBJECT_LIMIT_JOB_MEMORY |
                JOB_OBJECT_LIMIT_PROCESS_MEMORY |
                JOB_OBJECT_LIMIT_WORKINGSET;
        }

        eInfo.BasicLimitInformation.LimitFlags |=
            JOB_OBJECT_LIMIT_DIE_ON_UNHANDLED_EXCEPTION |
            JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;

        if (!SetInformationJobObject(hJob, JobObjectExtendedLimitInformation, &eInfo, sizeof(eInfo)))
            Subprocess_PushError(self, EID_SET_EXTENDED_LIMIT_INFO);
    }

    if (self->RestrictUI) {
        JOBOBJECT_BASIC_UI_RESTRICTIONS uInfo = { 0 };

        uInfo.UIRestrictionsClass =
            JOB_OBJECT_UILIMIT_DESKTOP |
            JOB_OBJECT_UILIMIT_DISPLAYSETTINGS |
            JOB_OBJECT_UILIMIT_EXITWINDOWS |
            JOB_OBJECT_UILIMIT_GLOBALATOMS |
            JOB_OBJECT_UILIMIT_HANDLES |
            JOB_OBJECT_UILIMIT_READCLIPBOARD |
            JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS |
            JOB_OBJECT_UILIMIT_WRITECLIPBOARD;

        if (!SetInformationJobObject(hJob, JobObjectBasicUIRestrictions, &uInfo, sizeof(uInfo)))
            Subprocess_PushError(self, EID_SET_UI_RESTRICTIONS);
    }

    return hJob;
}

static const uint64_t __pure__ FiletimeToUint64(const FILETIME * const ft) {
    /*LARGE_INTEGER li = {
    .u = {
    .LowPart = ft->dwLowDateTime,
    .HighPart = ft->dwHighDateTime,
    },
    };*/
    LARGE_INTEGER li = { 0 };
    li.u.LowPart = ft->dwLowDateTime;
    li.u.HighPart = ft->dwHighDateTime;
    return li.QuadPart / 10;
}

static bool UpdateProcessTimes(
struct Subprocess * const self,
    const JOBOBJECT_BASIC_ACCOUNTING_INFORMATION * const jInfo,
    const int bFinished) {

        FILETIME ftCreation, ftEnd, ftUser, ftKernel;
        SYSTEMTIME stCurrentTime;

        if (!GetProcessTimes(self->hProcess, &ftCreation, &ftEnd, &ftKernel, &ftUser)) {
            Subprocess_PushError(self, EID_GET_PROCESS_TIMES);
            return false;
        }

        if (!bFinished) {
            GetSystemTime(&stCurrentTime);
            if (unlikely(!SystemTimeToFileTime(&stCurrentTime, &ftEnd))) {
                Subprocess_PushError(self, EID_SYSTEM_TIME_TO_FILE_TIME);
                return false;
            }
        }

        self->srResult.ttWall = FiletimeToUint64(&ftEnd) - FiletimeToUint64(&ftCreation);

        if (jInfo) {
            self->srResult.ttUser = jInfo->TotalUserTime.QuadPart / 10;
            self->srResult.ttKernel = jInfo->TotalKernelTime.QuadPart / 10;
        } else {
            self->srResult.ttUser = FiletimeToUint64(&ftUser);
            self->srResult.ttKernel = FiletimeToUint64(&ftKernel);
        }

        return true;
}

static bool UpdateBasicProcessAccounting(struct Subprocess * const self, const int bFinished) {
    JOBOBJECT_BASIC_ACCOUNTING_INFORMATION jInfo = { 0 };

    if (self->hJob != INVALID_HANDLE_VALUE) {
        if (!QueryInformationJobObject(self->hJob, JobObjectBasicAccountingInformation, &jInfo, sizeof(jInfo), NULL)) {
            Subprocess_PushError(self, EID_QUERY_JOB_OBJECT);
            return false;
        }

        self->srResult.TotalProcesses = jInfo.TotalProcesses;
    }

    return UpdateProcessTimes(
        self,
        (self->hJob != INVALID_HANDLE_VALUE) ? &jInfo : NULL,
        bFinished);
}

static uint64_t GetProcessMemoryUsage(const HANDLE handle, const BOOL accountWorkingSet) {
    PROCESS_MEMORY_COUNTERS_EX pmc = {0};
    pmc.cb = sizeof pmc;

    if (unlikely(!GetProcessMemoryInfo(handle, (PPROCESS_MEMORY_COUNTERS) &pmc, sizeof pmc)))
        return 0;

    if (!accountWorkingSet) {
        return max(pmc.PeakPagefileUsage, pmc.PrivateUsage);
    } else {
        return pmc.PeakWorkingSetSize;
    }
}

static bool UpdateExtendedProcessAccounting(struct Subprocess * const self) {
    JOBOBJECT_EXTENDED_LIMIT_INFORMATION jInfo = { 0 };

    if (self->hJob != INVALID_HANDLE_VALUE) {
        if (!QueryInformationJobObject(self->hJob, JobObjectExtendedLimitInformation, &jInfo, sizeof(jInfo), NULL)) {
            Subprocess_PushError(self, EID_QUERY_JOB_OBJECT);
            return false;
        }

        if (!self->AccountWorkingSet) {
            self->srResult.PeakMemory = jInfo.PeakJobMemoryUsed;
        } else {
            self->srResult.PeakMemory = GetProcessMemoryUsage(self->hProcess, TRUE);
        }
    } else
        self->srResult.PeakMemory = GetProcessMemoryUsage(self->hProcess, self->AccountWorkingSet);

    return true;
}

static const bool __const__ IsTimeLimitHit(const struct Subprocess * const self) {
    return ((self->TimeLimit > 0) &&
        (self->srResult.ttUser + self->srResult.ttKernel >= self->TimeLimit));
}

static DWORD WINAPI SubprocessBottomHalf(LPVOID lpbb) {
    struct Subprocess * const self = (struct Subprocess *) lpbb;
    DWORD iWaitResult = 0;
    uint64_t ttLast = 0;
    uint64_t ttLastNew = 0;
    enum REDIRECTION_KEY x;

    while ((!self->srResult.SuccessCode) &&
        ((iWaitResult = WaitForSingleObject(
        self->hProcess, self->TimeQuantum)) == WAIT_TIMEOUT)) {
            UpdateBasicProcessAccounting(self, false);

            ttLastNew = self->srResult.ttKernel + self->srResult.ttUser;

            if (self->CheckIdleness) {
                if (ttLastNew == ttLast) {
                    self->IdleQuanta++;
                } else {
                    self->IdleQuanta = 0;
                }

                if (self->IdleQuanta >= QUANTA_FOR_IDLENESS) {
                    self->srResult.SuccessCode |= EF_INACTIVE;
                }
            }

            if (self->TimeLimit && IsTimeLimitHit(self))
                self->srResult.SuccessCode |= EF_TIME_LIMIT_HIT;

            if (self->HardTimeLimit && (self->srResult.ttWall >= self->HardTimeLimit))
                self->srResult.SuccessCode |= EF_TIME_LIMIT_HARD;

            ttLast = ttLastNew;

            if (self->ProcessLimit && (self->srResult.TotalProcesses > self->ProcessLimit))
                self->srResult.SuccessCode |= EF_PROCESS_LIMIT_HIT;

            if (self->MemoryLimit) {
                UpdateExtendedProcessAccounting(self);
                if (self->srResult.PeakMemory > self->MemoryLimit)
                    self->srResult.SuccessCode |= EF_MEMORY_LIMIT_HIT;
            }
    }

    switch (iWaitResult) {
    case WAIT_OBJECT_0:
        {
            DWORD dwExitCode;
            GetExitCodeProcess(self->hProcess, &dwExitCode);
            self->srResult.ExitCode = dwExitCode;
        }
        break;

    case WAIT_TIMEOUT:
        do {
            TerminateProcess(self->hProcess, 0);
        } while (WaitForSingleObject(self->hProcess, 100) == WAIT_TIMEOUT);
        self->srResult.SuccessCode |= EF_KILLED; // EX_KILLED
    }

    UpdateBasicProcessAccounting(self, true);
    UpdateExtendedProcessAccounting(self);

    ClearHandle(&self->hProcess);
    ClearHandle(&self->hJob);

    if (IsTimeLimitHit(self))
        self->srResult.SuccessCode |= EF_TIME_LIMIT_HIT_POST;

    if ((self->MemoryLimit > 0) &&
        (self->srResult.PeakMemory > self->MemoryLimit))
        self->srResult.SuccessCode |= EF_MEMORY_LIMIT_HIT_POST;

    if (self->ProcessLimit && (self->srResult.TotalProcesses > self->ProcessLimit))
        self->srResult.SuccessCode |= EF_PROCESS_LIMIT_HIT_POST;

    for_each_redirect(x)
        RedirectParameters_Wait(self->rp[x]);

    //UnloadUserProfile(self->hUser, self->hProfile);
    ClearHandle(&self->hUser);
    //ClearHandle(&self->hProfile);
    if (self->hDesktop != INVALID_HANDLE_VALUE)
        CloseDesktop(self->hDesktop);
    if (self->hWindowStation != INVALID_HANDLE_VALUE)
        CloseWindowStation(self->hWindowStation);

    self->hDesktop = self->hWindowStation = INVALID_HANDLE_VALUE;

    if (RedirectParameters_IsOverflow(self->rp[Output]))
        self->srResult.SuccessCode |= EF_STDOUT_OVERFLOW;
    if (RedirectParameters_IsOverflow(self->rp[Error]))
        self->srResult.SuccessCode |= EF_STDERR_OVERFLOW;

    if (self->cb)
        self->cb(self, self->cbarg);
    return 0;
}

int USED Subprocess_Launch(struct Subprocess * const self) {
    PROCESS_INFORMATION piProcessInfo = { 0 };
    const DWORD bResult = LaunchProcess(self, &piProcessInfo);
    {
        const DWORD dwError = GetLastError();
        ExecuteParameters_Clear(self);
        Redirects_CloseProcessEnd(self);

        if (!bResult) {
            Redirects_CloseClear(self);
            SetLastError(dwError);
            Subprocess_PushError(self, EID_LAUNCH_PROCESS);
            return 0;
        }
    }

    self->hProcess = piProcessInfo.hProcess;
    self->hThread = piProcessInfo.hThread;

    return -1;
}

static void Subprocess_InjectDll(struct Subprocess * const self, const WCHAR * const fileName) {
    DWORD dwProcessId = GetProcessId(self->hProcess);

    if (!InjectLibW(dwProcessId, fileName)) {
        Subprocess_PushError(self, EID_INJECT_DLL);
    }
}

static void Subprocess_AssignToJob(struct Subprocess * const self) {
    if (!self->NoJob) {
        self->hJob = CreateSubprocessJob(self);
        if (self->hJob != INVALID_HANDLE_VALUE) {
            if (!AssignProcessToJobObject(self->hJob, self->hProcess)) {
                Subprocess_PushError(self, EID_ASSIGN_TO_JOB);
                ClearHandle(&self->hJob);
            }
        } else
            Subprocess_PushError(self, EID_CREATE_JOB);
    }
}

static int Subprocess_StartThreads(struct Subprocess * const self) {
    enum REDIRECTION_KEY x;
    for_each_redirect(x)
        RedirectParameters_CreateThread(self->rp[x]);

    ResumeThread(self->hThread);
    CloseHandle(self->hThread);

    self->bhThread = CreateThread(NULL, 0, SubprocessBottomHalf, self, 0, NULL);
    return -1;
}

int USED Subprocess_StartEx(struct Subprocess * const self) {
    Subprocess_AssignToJob(self);
    Subprocess_StartThreads(self);
    return -1;
}

int USED Subprocess_Start(struct Subprocess * const self) {
    if (unlikely(!Subprocess_Launch(self)))
        return 0;

    if (self->wInjectDll)
        Subprocess_InjectDll(self, self->wInjectDll);

    Subprocess_StartEx(self);

    return -1;
}

int USED Subprocess_Wait(struct Subprocess * const self) {
    if (unlikely(!self))
        return 0;

    if (self->bhThread != INVALID_HANDLE_VALUE)
        WaitForSingleObject(self->bhThread, INFINITE);
    return -1;
}

int USED Subprocess_WaitFor(struct Subprocess * const self, unsigned int timeout) {
    if (unlikely(!self))
        return 0;

    if (self->bhThread != INVALID_HANDLE_VALUE) {
        DWORD waitRes = WaitForSingleObject(self->bhThread, (DWORD)timeout);
        if (waitRes != WAIT_OBJECT_0) {
            return 0;
        }
    }
    return -1;
}

void USED Subprocess_Terminate(struct Subprocess * const self) {
    if (unlikely(!self))
        return;

    if (self->hProcess && self->hProcess != INVALID_HANDLE_VALUE)
        TerminateProcess(self->hProcess, 0);
}

const struct SubprocessResult * const USED Subprocess_GetResult(const struct Subprocess * const self) {
    if (unlikely(!self))
        return NULL;
    return &self->srResult;
}


const struct SubprocessPipeBuffer * const USED Subprocess_GetRedirectBuffer(const struct Subprocess * const self, enum REDIRECTION_KEY key) {
    if (unlikely(!self))
    return NULL;
    return RedirectParameters_GetPipeBuffer(self->rp[key]);
}
