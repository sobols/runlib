#pragma once

#include "mystdbool.h"

struct RedirectParameters;

struct RedirectParameters * RedirectParameters_Alloc();

bool RedirectParameters_CreateThread(struct RedirectParameters * const rp);
void RedirectParameters_CloseProcessEnd(struct RedirectParameters * const rp);
int RedirectParameters_Wait(struct RedirectParameters * const rp);
void RedirectParameters_Clear(struct RedirectParameters ** const rp);
const HANDLE RedirectParameters_GetPipeHandle(const struct RedirectParameters * const rp);
bool RedirectParameters_KillThread(struct RedirectParameters * const rp);

const bool RedirectParameters_IsOverflow(const struct RedirectParameters * const rp) __pure__;

bool RedirectParameters_SetupFileRedirection(
    struct RedirectParameters * const self,
    const WCHAR * const wFileName,
    const bool bRead);

bool RedirectParameters_SetupBufferRedirection(
    struct RedirectParameters * const self, const bool bRead);

const struct SubprocessPipeBuffer * const RedirectParameters_GetPipeBuffer(
    const struct RedirectParameters * const self) __pure__;
