#pragma once

#include <optional>

#include <vector>
#include <string>
#include <ostream>

struct Subprocess;

namespace NRunLib {

    class TCommand {
    public:
        struct TAuthorizationParams {
            std::optional<std::string> Login;
            std::optional<std::string> Domain;
            std::optional<std::string> Password;
        };
        enum ESecurityMode {
            None,
            DropRights,
            CustomUser,
            CustomUserDropRights,
        };
        enum EMemoryAccountingMode {
            CommitCharge,
            WorkingSetSize
        };

    public:
        std::optional<std::string> CommandLine;
        std::optional<std::string> Directory;
        
        std::optional<long long> TimeLimit;
        std::optional<long long> MemoryLimit;
        
        std::optional<std::string> StdInFile;
        std::optional<std::string> StdErrFile;
        std::optional<std::string> StdOutFile;
        
        ESecurityMode SecurityMode;
        TAuthorizationParams AuthorizationParams;
        
        EMemoryAccountingMode MemoryAccountingMode;
        
        bool IsEnvironmentOverridden;
        std::vector<std::pair<std::string, std::string> > EnvironmentVariables;

        bool IsTrustedProcess;
        bool IsIdlenessChecking;

    public:
        TCommand();
    };

    std::ostream& operator <<(std::ostream& stream, const TCommand::ESecurityMode& mode);
    std::ostream& operator <<(std::ostream& stream, const TCommand::EMemoryAccountingMode& mode);
    std::ostream& operator <<(std::ostream& stream, const TCommand& command);

    class TResult {
    public:
        enum EOutcome {
            Ok,
            TimeLimitExceeded,
            MemoryLimitExceeded,
            IdlenessLimitExceeded,
            RuntimeError,
            SecurityViolation,
            Fail
        };

        EOutcome Outcome;
        long long TimeUsed;
        long long MemoryUsed;
        int ExitCode;
        std::optional<std::string> Message;

    public:
        TResult();
    };

    std::ostream& operator <<(std::ostream& stream, const TResult::EOutcome& outcome);
    std::ostream& operator <<(std::ostream& stream, const TResult& result);

    class TRunner {
    public:
        TRunner(const TCommand& command);
        ~TRunner();

        std::optional<TResult> WaitForResult(); // no result if the process was Kill()'ed.
        std::optional<TResult> WaitForResult(int timeout);
        void Kill();
    private:
        Subprocess* Process;
        std::optional<TResult> Result;
        bool WasKilled;
    private:
        std::optional<TResult> WaitForResultInternal(int timeout);
    };

    TResult Run(const TCommand& command);

} // namespace NRunLib
