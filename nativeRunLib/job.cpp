#include "job.h"

namespace NRunLib2 {
    namespace {
        TIocpKey ToIocpKey(PVOID ptr) {
            return reinterpret_cast<TIocpKey>(ptr);
        }
        TIocpKey ToIocpKey(ULONG_PTR ptr) {
            static_assert(sizeof(TIocpKey) <= sizeof(ULONG_PTR), "ULONG_PTR does not fit into TIocpKey");
            return static_cast<TIocpKey>(ptr);
        }
    }

    TIoCompletionPort::TIoCompletionPort()
        : Handle_{::CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, 0, 1)}
    {
        if (!Handle_.IsValid()) {
            throw TWindowsError{"CreateIoCompletionPort"};
        }
    }

    std::optional<TIoCompletionPort::TCompletionStatus> TIoCompletionPort::GetNext(DWORD dwMilliseconds) {
        DWORD completionCode;
        ULONG_PTR completionKey;
        LPOVERLAPPED overlapped;

        if (::GetQueuedCompletionStatus(*Handle_, &completionCode, &completionKey, &overlapped, dwMilliseconds)) {
            return TCompletionStatus{completionCode, ToIocpKey(completionKey)};
        }
        // GetQueuedCompletionStatus failed
        const DWORD error = ::GetLastError();
        if (!(error == ERROR_ABANDONED_WAIT_0 || error == WAIT_TIMEOUT)) {
            throw TWindowsError{"GetQueuedCompletionStatus"};
        }
        return std::nullopt;
    }

    TJobObject::TJobObject()
        : Handle_(::CreateJobObjectW(NULL, NULL))
    {
        if (!Handle_.IsValid()) {
            throw TWindowsError{"CreateJobObject"};
        }
    }

    void TJobObject::SetLimits(std::optional<uint32_t> activeProcessLimit) {
        JOBOBJECT_EXTENDED_LIMIT_INFORMATION eli = {};
        if (activeProcessLimit) {
            eli.BasicLimitInformation.ActiveProcessLimit = *activeProcessLimit;
            eli.BasicLimitInformation.LimitFlags |= JOB_OBJECT_LIMIT_ACTIVE_PROCESS;
        }
        eli.BasicLimitInformation.LimitFlags |=
            JOB_OBJECT_LIMIT_DIE_ON_UNHANDLED_EXCEPTION |
            JOB_OBJECT_LIMIT_KILL_ON_JOB_CLOSE;
        if (!::SetInformationJobObject(*Handle_, JobObjectExtendedLimitInformation, &eli, sizeof(eli))) {
            throw TWindowsError{"SetInformationJobObject", __func__};
        }
    }

    void TJobObject::SetUiRestrictions() {
        JOBOBJECT_BASIC_UI_RESTRICTIONS bur = {};
        bur.UIRestrictionsClass =
            JOB_OBJECT_UILIMIT_DESKTOP |
            JOB_OBJECT_UILIMIT_DISPLAYSETTINGS |
            JOB_OBJECT_UILIMIT_EXITWINDOWS |
            JOB_OBJECT_UILIMIT_GLOBALATOMS |
            JOB_OBJECT_UILIMIT_HANDLES |
            JOB_OBJECT_UILIMIT_READCLIPBOARD |
            JOB_OBJECT_UILIMIT_SYSTEMPARAMETERS |
            JOB_OBJECT_UILIMIT_WRITECLIPBOARD;
        if (!::SetInformationJobObject(*Handle_, JobObjectBasicUIRestrictions, &bur, sizeof(bur))) {
            throw TWindowsError{"SetInformationJobObject", __func__};
        }
    }

    TIocpKey TJobObject::AssociateCompletionPort(TIoCompletionPort& cp) {
        // We set the completion key to be the job itself.
        static_assert(sizeof(PVOID) <= sizeof(HANDLE), "HANDLE does not fit into IOCP key");
        PVOID key = *Handle_;
        JOBOBJECT_ASSOCIATE_COMPLETION_PORT acp = {};
        acp.CompletionKey = key;
        acp.CompletionPort = cp.Handle();
        if (!::SetInformationJobObject(*Handle_, JobObjectAssociateCompletionPortInformation, &acp, sizeof(acp))) {
            throw TWindowsError{"SetInformationJobObject", __func__};
        }
        return ToIocpKey(key);
    }

    void TJobObject::Assign(TProcess& process) {
        if (!::AssignProcessToJobObject(*Handle_, process.Handle())) {
            throw TWindowsError{"AssignProcessToJobObject"};
        }
    }

    void TJobObject::Terminate() {
        // Not mentioned in the official docs, but `TerminateJobObject` is suspected to be asynchronous,
        // meaning that it may return before all processes in the job are terminated.
        // https://stackoverflow.com/questions/23173804/is-terminatejobobject-asynchronous
        if (!::TerminateJobObject(*Handle_, 0)) {
            throw TWindowsError{"TerminateJobObject"};
        }
    }

    TCurrentTime TJobObject::GetTime() const {
        JOBOBJECT_BASIC_ACCOUNTING_INFORMATION bai = {};
        if (!::QueryInformationJobObject(*Handle_, JobObjectBasicAccountingInformation, &bai, sizeof(bai), NULL)) {
            throw TWindowsError{"QueryInformationJobObject"};
        }
        return {bai.TotalUserTime, bai.TotalKernelTime};
    }

    TCurrentMemory TJobObject::GetMemory() const {
        JOBOBJECT_EXTENDED_LIMIT_INFORMATION eli = {};

        if (!::QueryInformationJobObject(*Handle_, JobObjectExtendedLimitInformation, &eli, sizeof(eli), NULL)) {
            throw TWindowsError{"QueryInformationJobObject"};
        }
        return {eli.PeakJobMemoryUsed, 0};
    }

    TJobObject CreateJob(std::optional<uint32_t> activeProcessLimit) {
        TJobObject job;
        job.SetLimits(activeProcessLimit);
        job.SetUiRestrictions();
        return job;
    }
}
