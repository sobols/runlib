#pragma once

#include <sstream>
#include <stdexcept>


#define THROW_CUSTOM_EX(exType, msg)     \
do {                                     \
    std::ostringstream oss;              \
    oss << msg;                          \
    throw exType(oss.str());             \
    __pragma(warning(push))              \
    __pragma(warning(disable:4127))      \
    } while(0)                           \
    __pragma(warning(pop))

#define THROW_EX(msg) THROW_CUSTOM_EX(std::runtime_error, msg)

class TWindowsError: public std::runtime_error {
public:
    explicit TWindowsError(const char* apiFunctionName);
    TWindowsError(const char* apiFunctionName, const char* functionName);
    std::string VerboseWhat() const;

private:
    const char* ApiFunctionName_;
    const char* FunctionName_;
    uint32_t LastError_;
};

class TCreateProcessError : public TWindowsError {
    using TWindowsError::TWindowsError;
};

class TConfigurationError : public std::runtime_error {
    using std::runtime_error::runtime_error;
};
