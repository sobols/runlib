#include "errormode.h"

#include <Windows.h>

namespace NRunLib2 {
    TErrorModeGuard::TErrorModeGuard()
        : Mode_{::GetErrorMode()}
    {
        Mode_ = ::SetErrorMode(Mode_ | SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX);
    }

    TErrorModeGuard::~TErrorModeGuard() {
        ::SetErrorMode(Mode_);
    }

    void DisableErrorReportingDialog() {
        static TErrorModeGuard g;
    }
}
