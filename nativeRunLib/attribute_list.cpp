#include "attribute_list.h"

#include "exception.h"

namespace NRunLib2 {
    constexpr DWORD MAX_ATTRIBUTE_COUNT = 1;

    TProcThreadAttributeList::TProcThreadAttributeList() {
        SIZE_T size = 0;
        // It is expected to fail
        if (::InitializeProcThreadAttributeList(NULL, MAX_ATTRIBUTE_COUNT, 0, &size) ||
            ::GetLastError() != ERROR_INSUFFICIENT_BUFFER)
        {
            throw TWindowsError{"InitializeProcThreadAttributeList"};
        }

        LPVOID ptr = ::HeapAlloc(::GetProcessHeap(), 0, size);
        if (ptr == nullptr) {
            throw TWindowsError{"HeapAlloc"};
        }

        List_ = reinterpret_cast<LPPROC_THREAD_ATTRIBUTE_LIST>(ptr);

        if (!::InitializeProcThreadAttributeList(List_, MAX_ATTRIBUTE_COUNT, 0, &size)) {
            throw TWindowsError{"InitializeProcThreadAttributeList"};
        }
    }

    TProcThreadAttributeList::~TProcThreadAttributeList() {
        ::DeleteProcThreadAttributeList(List_);
        ::HeapFree(::GetProcessHeap(), 0, List_);
    }

    void TProcThreadAttributeList::SetHandlesToInherit(std::vector<HANDLE>&& handles) {
        // Store the list of handles in the class field because
        // the memory is referenced during the CreateProcess call
        Handles_ = std::move(handles);

        if (Handles_.empty()) {
            // The PROC_THREAD_ATTRIBUTE_HANDLE_LIST list cannot be empty;
            // the UpdateProcThreadAttribute call fails if cbSize is 0.
            // However, a list containing a NULL is apparently OK and equivalent to an empty list.
            Handles_ = {NULL};
        }

        if (!::UpdateProcThreadAttribute(List_,
            0, PROC_THREAD_ATTRIBUTE_HANDLE_LIST,
            Handles_.data(),
            Handles_.size() * sizeof(HANDLE), NULL, NULL))
        {
            throw TWindowsError{"UpdateProcThreadAttribute"};
        }
    }
}
