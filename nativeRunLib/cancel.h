#pragma once

#include <memory>

namespace NRunLib2 {
    class ICancellationToken {
    public:
        virtual ~ICancellationToken() = default;
        virtual bool IsCancellationRequested() const = 0;
        virtual void Cancel() = 0;
    };

    std::unique_ptr<ICancellationToken> CreateCancellationToken();
}
