#pragma once

#include "handle.h"

#include <Windows.h>

#include <memory>

namespace NRunLib2 {
    struct TSidDeleter {
        using pointer = PSID;
        void operator()(PSID h) {
            ::LocalFree(h);
        }
    };
    using TSidPtr = std::unique_ptr<PSID, TSidDeleter>;

    struct TSecurityDescriptorDeleter {
        using pointer = PSECURITY_DESCRIPTOR;
        void operator()(PSECURITY_DESCRIPTOR h) {
            ::LocalFree(h);
        }
    };
    using TSecurityDescriptorPtr = std::unique_ptr<PSECURITY_DESCRIPTOR, TSecurityDescriptorDeleter>;

    TSidPtr SidFromString(LPCWSTR sidString);
    TSecurityDescriptorPtr SecurityDescriptorFromString(LPCWSTR sdString);

    TScopedHandle MakeNormalToken();
    TScopedHandle MakeRestrictedToken();

    void SetLowIntegrityLevel(HANDLE token);
}
