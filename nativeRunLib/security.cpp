#include "security.h"

#include <sddl.h>
#include <WinSafer.h>

namespace NRunLib2 {
    TSidPtr SidFromString(LPCWSTR sidString) {
        PSID pSid;
        if (!::ConvertStringSidToSidW(sidString, &pSid)) {
            throw TWindowsError{"ConvertStringSidToSid"};
        }
        return TSidPtr{pSid};
    }

    TSecurityDescriptorPtr SecurityDescriptorFromString(LPCWSTR sdString) {
        PSECURITY_DESCRIPTOR pSD;
        if (!::ConvertStringSecurityDescriptorToSecurityDescriptorW(sdString, SDDL_REVISION_1, &pSD, NULL)) {
            throw TWindowsError{"ConvertStringSecurityDescriptorToSecurityDescriptor"};
        }
        return TSecurityDescriptorPtr{pSD};
    }

    TScopedHandle MakeNormalToken() {
        HANDLE hToken = NULL;
        if (!::OpenProcessToken(GetCurrentProcess(),
            TOKEN_DUPLICATE |
            TOKEN_ADJUST_DEFAULT |
            TOKEN_QUERY |
            TOKEN_ASSIGN_PRIMARY,
            &hToken))
        {
            throw TWindowsError{"OpenProcessToken"};
        }

        HANDLE hNewToken = NULL;
        if (!::DuplicateTokenEx(hToken,
            0,
            NULL,
            SecurityImpersonation,
            TokenPrimary,
            &hNewToken))
        {
            ::CloseHandle(hToken);
            throw TWindowsError{"DuplicateTokenEx"};
        }

        ::CloseHandle(hToken);
        return TScopedHandle{hNewToken};
    }

    TScopedHandle MakeRestrictedToken() {
        SAFER_LEVEL_HANDLE hLevel = NULL;
        if (!::SaferCreateLevel(SAFER_SCOPEID_USER, SAFER_LEVELID_CONSTRAINED, SAFER_LEVEL_OPEN, &hLevel, NULL)) {
            throw TWindowsError{"SaferCreateLevel"};
        }

        HANDLE hRestrictedToken = NULL;
        if (!::SaferComputeTokenFromLevel(
            hLevel,             // SAFER Level handle
            NULL,               // NULL is current thread token
            &hRestrictedToken,  // Target token
            0,                  // No flags
            NULL))              // Reserved
        {
            ::SaferCloseLevel(hLevel);
            throw TWindowsError{"SaferComputeTokenFromLevel"};
        }

        if (!::SaferCloseLevel(hLevel)) {
            ::CloseHandle(hRestrictedToken);
            throw TWindowsError{"SaferCloseLevel"};
        }
        return TScopedHandle{hRestrictedToken};
    }

    void SetLowIntegrityLevel(HANDLE token) {
        // Low integrity SID
        // There is a bug in MS tutorial, as stated here: https://stackoverflow.com/a/10001420
        // alternatively, use CreateWellKnownSid(WinMediumLabelSid) instead...
        TSidPtr pIntegritySid = SidFromString(L"S-1-16-4096");

        TOKEN_MANDATORY_LABEL til = {};
        til.Label.Attributes = SE_GROUP_INTEGRITY;
        til.Label.Sid = pIntegritySid.get();
        if (!::SetTokenInformation(token,
            TokenIntegrityLevel,
            &til,
            sizeof(TOKEN_MANDATORY_LABEL) + GetLengthSid(pIntegritySid.get())))
        {
            throw TWindowsError{"SetTokenInformation"};
        }
    }
}
