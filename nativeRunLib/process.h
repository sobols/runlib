#pragma once

#include "handle.h"
#include "metrics.h"

#include <string>
#include <optional>

namespace NRunLib2 {
    class TCommand;
    class TWindowsDesktop;

    class TProcess {
    public:
        TProcess(PROCESS_INFORMATION&& processInfo)
            : ProcessHandle_{processInfo.hProcess}
            , ThreadHandle_{processInfo.hThread}
        {
        }

        HANDLE Handle() const {
            return *ProcessHandle_;
        }

        void Resume() {
            const DWORD result = ::ResumeThread(*ThreadHandle_);
            if (result == DWORD(-1)) {
                throw TWindowsError{"ResumeThread"};
            }
            ThreadHandle_ = {};
        }

        void Terminate() {
            const BOOL result = ::TerminateProcess(*ProcessHandle_, 0);
            if (!result) {
                throw TWindowsError{"TerminateProcess"};
            }
        }

        uint32_t GetExitCode() const {
            DWORD exitCode = 0;
            const BOOL result = ::GetExitCodeProcess(*ProcessHandle_, &exitCode);
            if (!result) {
                throw TWindowsError{"GetExitCodeProcess"};
            }
            return exitCode;
        }

        void Join() {
            const DWORD result = ::WaitForSingleObject(*ProcessHandle_, INFINITE);
            if (result == WAIT_FAILED) {
                throw TWindowsError{"WaitForSingleObject"};
            }
            if (result != WAIT_OBJECT_0) {
                throw std::runtime_error{"Unable to wait for the process termination"};
            }
        }

        TCurrentTime GetTime() const {
            FILETIME ftCreation, ftEnd, ftUser, ftKernel;
            if (!::GetProcessTimes(Handle(), &ftCreation, &ftEnd, &ftKernel, &ftUser)) {
                throw TWindowsError{"GetProcessTimes"};
            }
            return {ftUser, ftKernel};
        }

        uint64_t GetClockCycles() const {
            ULONG64 cnt;
            if (!::QueryProcessCycleTime(*ProcessHandle_, &cnt)) {
                throw TWindowsError{"GetClockCycles"};
            }
            return cnt;
        }

        void SetAffinityMask(uint64_t mask) {
            DWORD_PTR processAffinityMask;
            DWORD_PTR systemAffinityMask;
            if (!::GetProcessAffinityMask(*ProcessHandle_, &processAffinityMask, &systemAffinityMask)) {
                throw TWindowsError{"GetProcessAffinityMask"};
            }
            if ((mask & systemAffinityMask) != mask) {
                throw TConfigurationError{"Invalid process affinity mask"};
            }
            if (!::SetProcessAffinityMask(*ProcessHandle_, mask)) {
                throw TWindowsError{"SetProcessAffinityMask"};
            }
        }

        TCurrentMemory GetMemory() const;

    private:
        TScopedHandle ProcessHandle_;
        TScopedHandle ThreadHandle_;
    };

    TProcess LaunchSuspendedProcess(const TCommand&);
}
