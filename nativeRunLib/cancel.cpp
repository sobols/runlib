#include "cancel.h"

#include <atomic>

namespace NRunLib2 {
    class TCancellationToken: public ICancellationToken {
    public:
        TCancellationToken()
            : Flag_{false}
        {
        }
        bool IsCancellationRequested() const override {
            return Flag_;
        }
        void Cancel() override {
            Flag_ = true;
        }
    private:
        std::atomic<bool> Flag_;
    };

    std::unique_ptr<ICancellationToken> CreateCancellationToken() {
        return std::make_unique<TCancellationToken>();
    }
}
