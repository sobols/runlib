#pragma once

namespace NRunLib2 {
    void CreateDirectoryAtLowIntegrity(const wchar_t* path);
    void RemoveDir(const wchar_t* path);
}
