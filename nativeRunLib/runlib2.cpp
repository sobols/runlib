#include "runlib2.h"

#include "cancel.h"
#include "common.h"
#include "desktop.h"
#include "exception.h"
#include "errormode.h"
#include "handle.h"
#include "job.h"
#include "metrics.h"
#include "process.h"
#include "redirect.h"

#include <cassert>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
#include <Windows.h>

namespace NRunLib2 {
    constexpr DWORD JOB_POLLING_PERIOD_MILLISECONDS = 40;

    EOutcome TResult::GetOutcome() const {
        if (ProcessLimitHit) {
            return EOutcome::SecurityViolation;
        }
        if (IdlenessLimitHit) {
            return EOutcome::IdlenessLimitExceeded;
        }
        if (TimeLimitHit) {
            return EOutcome::TimeLimitExceeded;
        }
        if (MemoryLimitHit) {
            return EOutcome::MemoryLimitExceeded;
        }
        return (ExitCode == 0) ? EOutcome::Ok : EOutcome::RuntimeError;
    }

    std::ostream& operator<<(std::ostream& out, EOutcome v) {
        switch (v) {
        case EOutcome::Ok:
            return out << "OK";
        case EOutcome::TimeLimitExceeded:
            return out << "TLE";
        case EOutcome::MemoryLimitExceeded:
            return out << "MLE";
        case EOutcome::IdlenessLimitExceeded:
            return out << "ILE";
        case EOutcome::RuntimeError:
            return out << "RTE";
        case EOutcome::SecurityViolation:
            return out << "SV";
        }
        return out;
    }

    class TProcessBox {
    private:
        const TCommand& Command_;
        TResult Result_;

        TJobObject Job_;
        TProcess Process_;
        std::optional<TIdleDetector> IdleDetector_;
        bool Dead_;
        bool Idle_;

    public:
        TProcessBox(const TCommand& command)
            : Command_{command}
            , Job_{CreateJob(command.ProcessLimit)}
            , Process_{LaunchSuspendedProcess(command)}
            , Dead_{false}
            , Idle_{false}
        {
            Job_.Assign(Process_);

            if (Command_.AffinityMask != 0) {
                Process_.SetAffinityMask(Command_.AffinityMask);
            }

            if (Command_.IdlenessLimit) {
                IdleDetector_ = {command.TimeLimit};
            }
        }

        void Resume() {
            Process_.Resume();
        }

        TResult FinalizeResult() {
            Process_.Join();
            Result_.ExitCode = Process_.GetExitCode();
            return Result_;
        }

        // Death is known for sure via a notification passed through IOCP from the job object
        bool IsDead() const {
            return Dead_;
        }

        bool IsIdle() const {
            return Idle_;
        }

        TIocpKey AssociateCompletionPort(TIoCompletionPort& cp) {
            return Job_.AssociateCompletionPort(cp);
        }

        void AcknowledgeIocpMessage(DWORD code) {
            if (code == JOB_OBJECT_MSG_ACTIVE_PROCESS_ZERO) {
                // https://devblogs.microsoft.com/oldnewthing/20130405-00/?p=4743
                Dead_ = true;
            }
            else if (code == JOB_OBJECT_MSG_ACTIVE_PROCESS_LIMIT) {
                Result_.ProcessLimitHit = true;
                // Kill?
            }
        }

        bool CheckLimitsExceeded(bool enableIdlenessLimit = true) {
            return CheckTimeLimit(enableIdlenessLimit) || CheckMemoryLimit();
        }

        bool CheckCanceled(bool cancelRequested) {
            if (cancelRequested) {
                Result_.Canceled = true;
                return true;
            }
            return false;
        }

        void SetIdlessLimitHit() {
            Result_.IdlenessLimitHit = true;
        }

        // Only request to die
        void KillAsync() {
            if (!Dead_ && !Result_.WasKilled) {
                Job_.Terminate();
                Result_.WasKilled = true;
            }
        }

    private:
        TCurrentTime GetElapsedTime() const {
            return (Command_.LimitAccountingMode == ELimitAccountingMode::PerProcess)
                ? Process_.GetTime()
                : Job_.GetTime();
        }

        uint64_t GetConsumedMemory() const {
            if (Command_.MemoryAccountingMode == EMemoryAccountingMode::WorkingSetSize) {
                return Process_.GetMemory().PeakWorkingSetSize;
            }
            else {
                const auto m = (Command_.LimitAccountingMode == ELimitAccountingMode::PerProcess)
                    ? Process_.GetMemory()
                    : Job_.GetMemory();
                return m.PeakCommitCharge;
            }
        }

        bool CheckTimeLimit(bool enableIdlenessLimit) {
            const auto t = GetElapsedTime();
            Result_.TimeUsed = std::max(Result_.TimeUsed, t.TotalTimeMs());
            if (Command_.TimeLimit && Result_.TimeUsed > *Command_.TimeLimit) {
                Result_.TimeLimitHit = true;
                return true;
            }
            // Detect idleness
            Idle_ = (IdleDetector_ && IdleDetector_->Check(t, Process_.GetClockCycles()));
            if (Idle_ && enableIdlenessLimit) {
                // Set ILE
                Result_.IdlenessLimitHit = true;
                return true;
            }
            return false;
        }

        bool CheckMemoryLimit() {
            uint64_t m = GetConsumedMemory();
            Result_.MemoryUsed = std::max(Result_.MemoryUsed, m);
            if (Command_.MemoryLimit && Result_.MemoryUsed > *Command_.MemoryLimit) {
                Result_.MemoryLimitHit = true;
                return true;
            }
            return false;
        }
    };

    namespace {
        void SetUpBeforeRunning() {
            DisableErrorReportingDialog();
        }

        TResult RunSingle(const TCommand& command, const ICancellationToken* ct) {
            SetUpBeforeRunning();

            TProcessBox box{ command };

            TIoCompletionPort iocp;
            const auto key = box.AssociateCompletionPort(iocp);

            box.Resume();

            while (!box.IsDead()) {
                if (auto result = iocp.GetNext(JOB_POLLING_PERIOD_MILLISECONDS)) {
                    if (result->Key == key) {
                        box.AcknowledgeIocpMessage(result->Code);
                    }
                }
                const bool cancelRequested = ct && ct->IsCancellationRequested();
                if (box.CheckLimitsExceeded() || box.CheckCanceled(cancelRequested)) {
                    box.KillAsync();
                }
            }

            return box.FinalizeResult();
        }

        TPipelineResult RunMultiple(const std::vector<TCommand>& commands, const ICancellationToken* ct) {
            SetUpBeforeRunning();
            const size_t n = commands.size();

            TPipelineResult unitedResult;
            unitedResult.Results.resize(n);
            unitedResult.OrderOfTermination.reserve(n);

            std::vector<TProcessBox> boxes;
            std::unordered_map<TIocpKey, size_t> key2idx;
            std::unordered_set<size_t> alive;
            boxes.reserve(n);
            key2idx.reserve(n);

            TIoCompletionPort iocp;
            for (size_t i = 0; i < n; ++i) {
                boxes.emplace_back(commands[i]);
                const auto key = boxes[i].AssociateCompletionPort(iocp);
                auto [_, inserted] = key2idx.emplace(key, i);
                assert(inserted);
            }

            for (size_t i = 0; i < n; ++i) {
                boxes[i].Resume();
                alive.insert(i);
            }

            while (!alive.empty()) {
                bool shouldKillAll = false;

                if (auto result = iocp.GetNext(JOB_POLLING_PERIOD_MILLISECONDS)) {
                    if (auto it = key2idx.find(result->Key); it != key2idx.end()) {
                        const size_t idx = it->second;

                        const bool deadBefore = boxes[idx].IsDead();
                        boxes[idx].AcknowledgeIocpMessage(result->Code);
                        const bool deadAfter = boxes[idx].IsDead();

                        if (!deadBefore && deadAfter) {
                            unitedResult.OrderOfTermination.emplace_back(idx);
                            unitedResult.Results[idx] = boxes[idx].FinalizeResult();
                            if (unitedResult.Results[idx].ExitCode != 0) {
                                shouldKillAll = true;
                            }
                            assert(alive.count(idx) == 1);
                            alive.erase(idx);
                        }
                    }
                }
                const bool cancelRequested = ct && ct->IsCancellationRequested();

                /**
                 * Idleness limit is a bit tricky. For example, in normal situation, while the solution
                 * performs intensive calculations, the interactor just waits for input and seems to be idle.
                 * So we set ILE only if _all_ processes do nothing.
                 */
                bool allProcessesAreIdle = true;
                for (auto& box : boxes) {
                    if (box.CheckLimitsExceeded(/*enableIdlenessLimit=*/false) || box.CheckCanceled(cancelRequested)) {
                        shouldKillAll = true;
                    }
                    if (!box.IsIdle()) {
                        allProcessesAreIdle = false;
                    }
                }

                if (!shouldKillAll && allProcessesAreIdle) {
                    shouldKillAll = true;
                    for (auto& box : boxes) {
                        box.SetIdlessLimitHit();
                    }
                }
                if (shouldKillAll) {
                    for (auto& box : boxes) {
                        box.KillAsync();
                    }
                }
            }

            assert(unitedResult.OrderOfTermination.size() == n);
            return unitedResult;
        }

        void LinkWithPipe(TCommand& producer, TCommand& consumer, TAnonymousPipe& pipe) {
            if (!producer.StdOutput.IsDefault() || !consumer.StdInput.IsDefault()) {
                throw TConfigurationError{ "stdin & stdout must not be set" };
            }
            producer.StdOutput.SetPipe(pipe);
            consumer.StdInput.SetPipe(pipe);
        }
    } // namespace

    TResult Run(const TCommand& command, const ICancellationToken* ct) {
        return RunSingle(command, ct);
    }

    TPipelineResult RunPipeline(std::vector<TCommand> commands, bool loop, const ICancellationToken* ct) {
        size_t n = commands.size();
        if (n == 0) {
            return {};
        }

        std::vector<TAnonymousPipe> pipes(loop ? n : (n - 1));
        for (size_t i = 0; i + 1 < n; ++i) {
            LinkWithPipe(commands[i], commands[i + 1], pipes[i]);
        }
        if (loop) {
            LinkWithPipe(commands.back(), commands.front(), pipes.back());
        }
        return RunMultiple(commands, ct);
    }

    bool CanRun(const TCommand& command) {
        try {
            TProcess process = LaunchSuspendedProcess(command);
            process.Terminate();
            process.Join();
            return true;
        } catch (const TCreateProcessError&) {
            return false;
        }
    }
}
