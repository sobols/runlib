#pragma once

#include <string>
#include <string_view>

double BytesToMegabytes(long long bytes, int precision = 1);

std::string WideCharToStdString(const wchar_t* source);
std::string WideCharToStdString(std::wstring_view source);
std::wstring StdStringToStdWstring(std::string_view source);

std::string GetLastErrorMessage(int errorCode);
std::string GetCurrentModuleDirectory();
std::string GetDosPath(const std::string& fullPath);
std::string GetEnvVariable(const wchar_t* name);
std::string GetTmpPath();
std::string GetWinPath();
