#include "exception.h"

#include "common.h"

#include <sstream>
#include <Windows.h>

TWindowsError::TWindowsError(const char* apiFunctionName, const char* functionName)
    : std::runtime_error{"TWindowsError"}
    , ApiFunctionName_{apiFunctionName}
    , FunctionName_{functionName}
    , LastError_{GetLastError()}
{
}

TWindowsError::TWindowsError(const char* apiFunctionName)
    : TWindowsError{apiFunctionName, nullptr}
{
}

std::string TWindowsError::VerboseWhat() const {
    std::ostringstream oss;
    oss << what() << ": " << ApiFunctionName_ << " failed";
    if (FunctionName_) {
        oss << "(at " << FunctionName_ << ")";
    }
    oss << ": " << GetLastErrorMessage(LastError_);
    return std::move(oss.str());
}
