#include "security.h"

namespace NRunLib2 {
    void CreateDirectoryAtLowIntegrity(const wchar_t* path) {
        // Allow all access for everyone: D:(A;OICI;GA;;;WD)
        auto pSD = SecurityDescriptorFromString(L"S:(ML;OICI;NW;;;LW)");

        SECURITY_ATTRIBUTES sa;
        sa.nLength = sizeof(SECURITY_ATTRIBUTES);
        sa.lpSecurityDescriptor = pSD.get();
        sa.bInheritHandle = FALSE;
        if (!CreateDirectoryW(path, &sa)) {
            throw TWindowsError{"CreateDirectoryW"};
        }
    }

    void RemoveDir(const wchar_t* cPath) {
        std::wstring path = cPath;
        path += L'\0';
        SHFILEOPSTRUCTW fileOp = {
            NULL,
            FO_DELETE,
            path.c_str(),
            L"",
            FOF_NOCONFIRMATION |
            FOF_NOERRORUI |
            FOF_SILENT,
            false,
            0,
            L""};
        SHFileOperationW(&fileOp);
    }
}
