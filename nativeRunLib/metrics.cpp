#include "metrics.h"

#include <algorithm>

namespace NRunLib2 {
    constexpr uint64_t IDLENESS_LIMIT = 1000 /* ms */;
    constexpr uint64_t HARD_MULTIPLIER = 10 /* TL x10 */;

    namespace {
        std::optional<uint64_t> GetHardTimeLimit(std::optional<uint64_t> timeLimit) {
            if (!timeLimit) {
                return std::nullopt;
            }
            return std::max(*timeLimit * HARD_MULTIPLIER, IDLENESS_LIMIT);
        }

        uint64_t TickCountDelta(uint64_t t1, uint64_t t2) {
            return (t2 > t1) ? (t2 - t1) : 0;
        }
    }

    TIdleDetector::TIdleDetector(std::optional<uint64_t> timeLimit)
        : HardTimeLimit_{GetHardTimeLimit(timeLimit)}
        , StartTickCount_{GetTickCount64()}
    {
    }

    bool TIdleDetector::Check(const TCurrentTime& procTime, uint64_t cycles) {
        const TState cur{
            ::GetTickCount64(),
            procTime.TotalTimeRaw(),
            cycles
        };

        if (HardTimeLimit_) {
            if (TickCountDelta(StartTickCount_, cur.TickCount) > *HardTimeLimit_) {
                return true;
            }
        }

        if (LastChange_
            && (LastChange_->ProcessTime == cur.ProcessTime)
            && (LastChange_->Cycles == cur.Cycles))
        {
            return TickCountDelta(LastChange_->TickCount, cur.TickCount) > IDLENESS_LIMIT;
        }

        // something has been changed
        LastChange_ = cur;
        return false;
    }
}
