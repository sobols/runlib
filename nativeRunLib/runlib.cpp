#include "stdafx.h"
#include "runlib.h"

#include "common.h"
#include "exception.h"
#include "contrib/include/w32invoke.h"

#include <cassert>
#include <cmath>
#include <vector>

namespace NRunLib {

    TCommand::TCommand()
        : SecurityMode(TCommand::DropRights)
        , MemoryAccountingMode(TCommand::CommitCharge)
        , IsTrustedProcess(false)
        , IsIdlenessChecking(false)
        , IsEnvironmentOverridden(true)
    {
    }

    TResult::TResult()
        : Outcome(TResult::Fail)
        , TimeUsed(0)
        , MemoryUsed(0)
        , ExitCode(0)
    {
    }

    namespace {
        void SetCommand(Subprocess* process, const TCommand& command) {
            if (command.CommandLine) {
                if (0 == Subprocess_SetStringUTF8(process, RUNLIB_COMMAND_LINE, command.CommandLine->c_str())) {
                    THROW_EX("can't set process command line to '" << *command.CommandLine << "'");
                }
            } else {
                THROW_EX("no command line is given");
            }

            if (command.Directory) {
                if (0 == Subprocess_SetStringUTF8(process, RUNLIB_CURRENT_DIRECTORY, command.Directory->c_str())) {
                    THROW_EX("can't set current directory to '" << *command.Directory << "'");
                }
            }
            
            // Limits
            if (command.TimeLimit) {
                if (0 == Subprocess_SetInt(process, RUNLIB_TIME_LIMIT, *command.TimeLimit * 1000LL)) {
                    THROW_EX("can't set time limit to " << *command.TimeLimit << " ms");
                }
            }
            if (command.MemoryLimit) {
                if (0 == Subprocess_SetInt(process, RUNLIB_MEMORY_LIMIT, *command.MemoryLimit)) {
                    THROW_EX("can't set memory limit to " << *command.TimeLimit << " bytes");
                }
            }

            // Redirections
            if (command.StdInFile) {
                if (0 == Subprocess_SetFileRedirectUTF8(process, Input, command.StdInFile->c_str())) {
                    THROW_EX("can't set stdin redirection to '" + *command.StdInFile + "'");
                }
            }
            if (command.StdOutFile) {
                if (0 == Subprocess_SetFileRedirectUTF8(process, Output, command.StdOutFile->c_str())) {
                    THROW_EX("can't set stdout redirection to '" + *command.StdOutFile + "'");
                }
            }
            if (command.StdErrFile) {
                if (0 == Subprocess_SetFileRedirectUTF8(process, Error, command.StdErrFile->c_str())) {
                    THROW_EX("can't set stderr redirection to '" + *command.StdErrFile + "'");
                }
            }

            if (command.SecurityMode == TCommand::CustomUser || command.SecurityMode == TCommand::CustomUserDropRights) {
                // Authorization
                const TCommand::TAuthorizationParams& ap = command.AuthorizationParams;
                if (ap.Login) {
                    if (0 == Subprocess_SetStringUTF8(process, RUNLIB_USERNAME, ap.Login->c_str())) {
                        THROW_EX("can't set login");
                    }
                }
                if (ap.Password) {
                    if (0 == Subprocess_SetStringUTF8(process, RUNLIB_PASSWORD, ap.Password->c_str())) {
                        THROW_EX("can't set password");
                    }
                }
                if (ap.Domain) {
                    if (0 == Subprocess_SetStringUTF8(process, RUNLIB_DOMAIN, ap.Domain->c_str())) {
                        THROW_EX("can't set domain");
                    }
                }
            }
            if (command.SecurityMode == TCommand::DropRights || command.SecurityMode == TCommand::CustomUserDropRights) {
                if (0 == Subprocess_SetBool(process, RUNLIB_DROP_RIGHTS, 1)) {
                    THROW_EX("can't set up 'drop my rights' mode");
                }
            }

            // Memory accounting mode
            if (command.MemoryAccountingMode == TCommand::WorkingSetSize) {
                if (0 == Subprocess_SetBool(process, RUNLIB_ACCOUNT_WORKING_SET, 1)) {
                    THROW_EX("can't set working set accounting mode");
                }
            }

            // Env vars
            if (command.IsEnvironmentOverridden) {
                std::string buffer;
                for (size_t i = 0; i < command.EnvironmentVariables.size(); ++i) {
                    const auto& p = command.EnvironmentVariables[i];
                    if (p.first.find('=') != std::string::npos) {
                        THROW_EX("env variable name '" << p.first << "' is not allowed: it contains equality sign");
                    }
                    if (p.first.empty()) {
                        THROW_EX("empty env variable name");
                    }
                    std::string current = p.first + "=" + p.second;
                    current += '\0';
                    buffer += current;
                }
                buffer += '\0';
                if (0 == Subprocess_SetBufferUTF8(process, RUNLIB_ENVIRONMENT, buffer.c_str(), static_cast<int>(buffer.length()))) {
                    THROW_EX("can't set env vars block");
                }
            }

            // Trusted
            if (!command.IsTrustedProcess) {
                if (0 == Subprocess_SetInt(process, RUNLIB_PROCESS_LIMIT, 1)) {
                    THROW_EX("can't set process limit to one process");
                }
                if (0 == Subprocess_SetBool(process, RUNLIB_RESTRICT_UI, 1)) {
                    THROW_EX("can't restrict UI functions");
                }
            }

            // Idleness
            if (command.IsIdlenessChecking) {
                if (0 == Subprocess_SetBool(process, RUNLIB_CHECK_IDLENESS, 1)) {
                    THROW_EX("can't set idleness checking");
                }
            }
        }

        void LaunchProcess(Subprocess* process) {
            if (0 == Subprocess_Start(process)) {
                THROW_EX("can't start process");
            }
            if (0 == Subprocess_Wait(process)) {
                THROW_EX("can't wait for process");
            }
        }

        std::vector<SubprocessErrorEntry> DumpRunLibErrors(Subprocess* process) {
            std::vector<SubprocessErrorEntry> errs;
            while (Subprocess_HasError(process)) {
                const SubprocessErrorEntry e = Subprocess_PopError(process);
                errs.push_back(e);
            }
            return errs;
        }

        std::string RepresentErrors(const std::vector<SubprocessErrorEntry>& errs) {
            std::ostringstream oss;
            if (!errs.empty()) {
                oss << "internal runlib errors:\n";
                for (size_t i = 0; i < errs.size(); ++i) {
                    const SubprocessErrorEntry& e = errs[i];
                    oss << "- Error: " << e.error_id << ", GetLastError = " << e.dwLastError << " [" << GetLastErrorMessage(e.dwLastError) << "]\n";
                }
            } else {
                oss << "no internal runlib errors";
            }
            return oss.str();
        }
        
        void FillInternalRunLibErrors(Subprocess* process, TResult* result) {
            const std::vector<SubprocessErrorEntry>& errs = DumpRunLibErrors(process);
            if (!errs.empty()) {
                // strict error handling
                result->Outcome = TResult::Fail;
                if (result->Message) {
                    *(result->Message) += "\n" + RepresentErrors(errs);
                } else {
                    result->Message = RepresentErrors(errs);
                }
            }
        }

        TResult GetResult(Subprocess* process) {
            TResult ret;
            const SubprocessResult* const invocationResult = Subprocess_GetResult(process);
            
            const int flags = invocationResult->SuccessCode;

            if (0 == flags) {
                if (invocationResult->ExitCode == 0) {
                    ret.Outcome = TResult::Ok;
                } else {
                    ret.Outcome = TResult::RuntimeError;
                }
            
            } else if (0 != (flags & EF_PROCESS_LIMIT_HIT) || 0 != (flags & EF_PROCESS_LIMIT_HIT_POST)) {
                ret.Outcome = TResult::SecurityViolation;
                std::ostringstream oss;
                oss << "process limit violated: " << invocationResult->TotalProcesses << " proc. created";
                ret.Message = oss.str();
            
            } else if (0 != (flags & EF_INACTIVE) || 0 != (flags & EF_TIME_LIMIT_HARD)) {
                ret.Outcome = TResult::IdlenessLimitExceeded;
            
            } else if (0 != (flags & EF_TIME_LIMIT_HIT) || 0 != (flags & EF_TIME_LIMIT_HIT_POST)) {
                ret.Outcome = TResult::TimeLimitExceeded;

            } else if (0 != (flags & EF_MEMORY_LIMIT_HIT) || 0 != (flags & EF_MEMORY_LIMIT_HIT_POST)) {
                ret.Outcome = TResult::MemoryLimitExceeded;

            } else {
                ret.Outcome = TResult::Fail;
            }

            if (ret.Outcome != TResult::Fail) {
                ret.ExitCode = invocationResult->ExitCode;
                //ret.TimeUsed = invocationResult->ttUser / 1000LL;
                ret.TimeUsed = (invocationResult->ttUser + invocationResult->ttKernel) / 1000LL;
                ret.MemoryUsed = invocationResult->PeakMemory;
            }
            return ret;
        }

        TResult RunImpl(const TCommand& command, Subprocess* process) {    
            SetCommand(process, command);
            LaunchProcess(process);
            TResult result = GetResult(process);
            return result;
        }
    } // namespace

    TResult Run(const TCommand& command)
    {
        TResult result;
        Subprocess* process = Subprocess_Create();

        if (process) {
            try {
                result = RunImpl(command, process);
            } catch (const std::exception& ex) {
                const char* what = ex.what();
                if (what) {
                    result.Message = std::string(what);
                }
            } catch (...) {
                result.Message = "unknown error (...)";
            }

            FillInternalRunLibErrors(process, &result);
            
            Subprocess_Destroy(process);
        } else {
            result.Message = "null process created";
        }
        return result;
    }


    // TRunner

    TRunner::TRunner(const TCommand& command)
        : Process(0)
        , WasKilled(false)
    {
        Process = Subprocess_Create();
        if (!Process) {
            Result = TResult();
            Result->Message = "null process created";
        } else {
            try {
                SetCommand(Process, command);
                if (0 == Subprocess_Start(Process)) {
                    THROW_EX("can't start process");
                }
            } catch (const std::exception& ex) {
                Result = TResult();
                const char* what = ex.what();
                if (what) {
                    Result->Message = std::string(what);
                }
            } catch (...) {
                Result = TResult();
                Result->Message = "unknown error (...)";
            }

            if (Result) {
                FillInternalRunLibErrors(Process, &Result.value());
            }
        }
        // 'Result' is not initialized here if no error occured
    }

    void TRunner::Kill()
    {
        if (WasKilled) {
            return;
        }
        if (Result) {
            return;
        }
        assert(!!Process);
        Subprocess_Terminate(Process);
        WasKilled = true;
    }

    std::optional<TResult> TRunner::WaitForResult()
    {
        return WaitForResultInternal(-1);
    }
    std::optional<TResult> TRunner::WaitForResult(int timeout)
    {
        if (timeout < 0) {
            timeout = 0;
        }
        return WaitForResultInternal(timeout);
    }
    std::optional<TResult> TRunner::WaitForResultInternal(int timeout)
    {
        if (Result || WasKilled) {
            // in case of WasKilled result may be boost::none
            return Result;
        }
        assert(!!Process);

        if (timeout == -1) {
            Subprocess_Wait(Process);
        } else {
            if (0 == Subprocess_WaitFor(Process, timeout)) {
                // no result yet
                return std::nullopt;
            }
        }

        // we have result here!
        try {
            Result = GetResult(Process);
        } catch (const std::exception& ex) {
            const char* what = ex.what();
            Result = TResult();
            if (what) {
                Result->Message = std::string(what);
            }
        } catch (...) {
            Result = TResult();
            Result->Message = "unknown error (...)";
        }

        assert(!!Result);
        FillInternalRunLibErrors(Process, &Result.value());

        return Result;
    }
    TRunner::~TRunner()
    {
        if (Process) {
            Subprocess_Destroy(Process);
            Process = 0;
        }
    }

    // Output

    std::ostream& operator <<(std::ostream& stream, const TCommand::ESecurityMode& mode) {
        // typedef TCommand::ESecurityMode SM;
        typedef TCommand SM;
        switch (mode) {
            case SM::None:                  return stream << "None";
            case SM::DropRights:            return stream << "Drop Rights";
            case SM::CustomUser:            return stream << "Custom User";
            case SM::CustomUserDropRights:  return stream << "Custom User, Drop Rights";
            default: THROW_EX("unable to print TCommand::ESecurityMode value " << static_cast<size_t>(mode));
        };
    }

    std::ostream& operator <<(std::ostream& stream, const TCommand::EMemoryAccountingMode& mode) {
        // typedef TCommand::EMemoryAccountingMode MAM;
        typedef TCommand MAM;
        switch (mode) {
            case MAM::CommitCharge:     return stream << "Commit Charge";
            case MAM::WorkingSetSize:   return stream << "Working Set Size";
            default: THROW_EX("unable to print TCommand::EMemoryAccountingMode value " << static_cast<size_t>(mode));
        };
    }

    std::ostream& operator <<(std::ostream& stream, const TCommand& command) {
        stream << " * ";
        if (command.TimeLimit) {
            stream << "TL " << *command.TimeLimit << " ms, ";
        }
        stream << (command.IsIdlenessChecking ? "checking idleness" : "no idleness check") << '\n';

        stream << " * ";
        if (command.MemoryLimit) {
            stream << "ML " << *command.MemoryLimit << " ~ " << BytesToMegabytes(*command.MemoryLimit) << " MB, ";
        }
        stream << "counting " << command.MemoryAccountingMode << '\n';

        stream << " * Security: " << command.SecurityMode << '\n';

        if (command.StdInFile) {
            stream << " * StdIn from " << *command.StdInFile << '\n';
        }
        if (command.StdOutFile) {
            stream << " * StdOut from " << *command.StdOutFile << '\n';
        }
        if (command.StdErrFile) {
            stream << " * StdErr from " << *command.StdErrFile << '\n';
        }
        
        if (command.Directory) {
            stream << " * Directory: " << *command.Directory << '\n';
        }

        if (command.IsTrustedProcess) {
            stream << " * Trusted mode ON" << '\n';
        }

        const TCommand::TAuthorizationParams& authParams = command.AuthorizationParams;
        if (authParams.Login || authParams.Password || authParams.Domain) {
            stream << " * Auth.: ";
            if (authParams.Login) {
                stream << "login " << *authParams.Login << ", ";
            }
            if (authParams.Password) {
                stream << "password " << std::string(authParams.Password->length(), '*') << ", ";
            }
            if (authParams.Domain) {
                stream << "domain " << *authParams.Domain << ", ";
            }
            stream << '\n';
        }

        if (command.IsEnvironmentOverridden) {
            stream << " * Env vars:\n";
            for (size_t i = 0; i < command.EnvironmentVariables.size(); ++i) {
                const auto& p = command.EnvironmentVariables[i];
                stream << p.first << "=" << p.second << std::endl;
            }
        }

        if (command.CommandLine) {
            stream << " * Command: " << *command.CommandLine;
        }
        return stream;
    }

    std::ostream& operator <<(std::ostream& stream, const TResult::EOutcome& outcome) {
        switch (outcome) {
            case TResult::Ok:                       return stream << "OK";
            case TResult::TimeLimitExceeded:        return stream << "TL";
            case TResult::MemoryLimitExceeded:      return stream << "ML";
            case TResult::IdlenessLimitExceeded:    return stream << "IL";
            case TResult::RuntimeError:             return stream << "RE";
            case TResult::SecurityViolation:        return stream << "SV";
            case TResult::Fail:                     return stream << "FAIL";
            default: THROW_EX("unable to print TResult::EOutcome value " << static_cast<size_t>(outcome));
        };
    }

    std::ostream& operator <<(std::ostream& stream, const TResult& result) {
        stream << " * Outcome: " << result.Outcome;
        stream << ", time: " << result.TimeUsed << ", memory: " << result.MemoryUsed << " ~ " << BytesToMegabytes(result.MemoryUsed) << " MB, code: " << result.ExitCode;
        if (result.Message) {
            stream << "\n * Message: " << *result.Message;
        }
        return stream;
    }

} // namespace NRunLib
