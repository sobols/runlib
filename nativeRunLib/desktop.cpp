#include "desktop.h"

#include "exception.h"

namespace NRunLib2 {
    namespace {
        std::wstring GetWindowObjectName(HANDLE handle) {
            // Get the size of the name.
            std::wstring result;

            DWORD size = 0;
            ::GetUserObjectInformationW(handle, UOI_NAME, NULL, 0, &size);
            if (size == 0) {
                return result;
            }

            assert(size % sizeof(wchar_t) == 0);
            size_t len = size / sizeof(wchar_t) - 1;
            result.resize(len);

            // Query the name of the object.
            if (!::GetUserObjectInformationW(handle, UOI_NAME, result.data(), size, &size)) {
                throw TWindowsError{"GetUserObjectInformation"};
            }

            return result;
        }

        class TDesktopGuard {
        public:
            TDesktopGuard()
                : OldDesktop_{::GetThreadDesktop(GetCurrentThreadId())}
            {
            }
            ~TDesktopGuard() {
                ::SetThreadDesktop(OldDesktop_);
            }
        private:
            HDESK OldDesktop_;
        };

        class TWindowStationGuard {
        public:
            TWindowStationGuard(HWINSTA newStation)
                : OldStation_{::GetProcessWindowStation()}
            {
                if (!::SetProcessWindowStation(newStation)) {
                    throw TWindowsError{"SetProcessWindowStation"};
                }
            }
            ~TWindowStationGuard() {
                ::SetProcessWindowStation(OldStation_);
            }
        private:
            HWINSTA OldStation_;
        };
    }

    TWindowsDesktop::TWindowsDesktop() {
        auto pSD = SecurityDescriptorFromString(L"S:(ML;;NW;;;LW)");
        SECURITY_ATTRIBUTES sa = {};
        sa.nLength = sizeof(SECURITY_ATTRIBUTES);
        sa.lpSecurityDescriptor = pSD.get();
        sa.bInheritHandle = FALSE;

        // MSDN: Only members of the Administrators group are allowed to specify a name
        WindowStation_.reset(::CreateWindowStationW(NULL, 0, MAXIMUM_ALLOWED, &sa));
        if (!WindowStation_) {
            throw TWindowsError{"CreateWindowStation"};
        }

        const std::wstring windowStationName = GetWindowObjectName(WindowStation_.get());
        const std::wstring desktopName = L"runlib_alt_desktop_" + std::to_wstring(GetCurrentProcessId());
        {
            TDesktopGuard desktopGuard;
            TWindowStationGuard windowStationGuard{WindowStation_.get()};

            Desktop_.reset(::CreateDesktopW(desktopName.c_str(), NULL, NULL, 0, GENERIC_ALL, &sa));
            if (!Desktop_) {
                throw TWindowsError{"CreateDesktop"};
            }
        }

        DesktopName_ = windowStationName + L'\\' + desktopName;
    }
}
