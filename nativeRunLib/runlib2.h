#pragma once

#include <optional>
#include <string>
#include <vector>

namespace NRunLib2 {
    class TAnonymousPipe;
    class ICancellationToken;

    /**
     * Enums
     */

    enum class ERedirectMode {
        // Leave as is
        Default,

        // /dev/null
        Null,

        // Input from file, output to file
        File,

        // Special value that can be used for standard error
        // and indicates that standard error should go into the same handle as standard output
        StdOutput,

        // Internal anonymous pipe (passed by pointer)
        Pipe,
    };

    enum class ELimitAccountingMode {
        PerProcess,
        PerJob, // system processes (i. e. conhost.exe) are also included
    };

    enum class EMemoryAccountingMode {
        CommitCharge,
        WorkingSetSize,
    };

    enum class ESecurityMode {
        None,
        LowIntegrity,
    };

    enum class EEnvironmentMode {
        Inherit,
        Override,
    };

    enum class EGuiMode {
        Normal,
        Hide,
    };

    enum class EProcessPriority {
        Default,
        RealTime,
        High,
        AboveNormal,
        Normal,
        BelowNormal,
        Idle,
    };

    /**
     * Configs
     */

    struct TRedirectConfig {
        ERedirectMode Mode = ERedirectMode::Default;
        std::string Value;
        TAnonymousPipe* PipePtr = nullptr;

        void SetFile(std::string path) {
            Mode = ERedirectMode::File;
            Value = std::move(path);
        }
        void SetNull() {
            Mode = ERedirectMode::Null;
            Value = {};
        }
        void SetPipe(TAnonymousPipe& pipe) {
            Mode = ERedirectMode::Pipe;
            PipePtr = &pipe;
        }
        bool IsDefault() const {
            return Mode == ERedirectMode::Default;
        }
    };

    struct TEnvVariable {
        std::string Name;
        std::string Value;
    };

    /**
     * Command
     */

    class TCommand {
    public:
        TCommand(std::string commandLine)
            : CommandLine{std::move(commandLine)}
            , EnvironmentMode{EEnvironmentMode::Inherit}
            , TimeLimit{1000}
            , MemoryLimit{1024 * 1024 * 1024}
            , ProcessLimit{1}
            , LimitAccountingMode{ELimitAccountingMode::PerProcess}
            , MemoryAccountingMode{EMemoryAccountingMode::CommitCharge}
            , SecurityMode{ESecurityMode::LowIntegrity}
            , GuiMode{EGuiMode::Hide}
            , IdlenessLimit{true}
            , AffinityMask{0}
            , ProcessPriority{EProcessPriority::Default}
        {
        }

    public:
        std::string CommandLine;

        std::optional<std::string> CurrentDirectory;
        EEnvironmentMode EnvironmentMode;
        std::vector<TEnvVariable> EnvVariables;
        uint64_t AffinityMask;
        EProcessPriority ProcessPriority;

        std::optional<uint64_t> TimeLimit;
        std::optional<uint64_t> MemoryLimit;
        std::optional<uint32_t> ProcessLimit;

        ELimitAccountingMode LimitAccountingMode;
        EMemoryAccountingMode MemoryAccountingMode;
        ESecurityMode SecurityMode;
        EGuiMode GuiMode;
        bool IdlenessLimit;

        TRedirectConfig StdInput;
        TRedirectConfig StdOutput;
        TRedirectConfig StdError;
    };

    /**
     * Result
     */

    enum class EOutcome {
        Ok,
        TimeLimitExceeded,
        MemoryLimitExceeded,
        IdlenessLimitExceeded,
        RuntimeError,
        SecurityViolation,
    };
    std::ostream& operator<<(std::ostream&, EOutcome);

    struct TResult {
        bool WasKilled = false;
        uint32_t ExitCode = 0;
        bool Canceled = false;
        bool TimeLimitHit = false;
        bool IdlenessLimitHit = false;
        bool MemoryLimitHit = false;
        bool ProcessLimitHit = false;

        uint64_t TimeUsed = 0;
        uint64_t MemoryUsed = 0;

        EOutcome GetOutcome() const;
    };

    /**
     * Executes the process on the calling thread.
     * Blocks until the child process has exited.
     * Either returns a result or throws an exception.
     */
    TResult Run(const TCommand& command, const ICancellationToken* ct = nullptr);

    /**
     * Executes a chain of N interacting processes simultaneously.
     * i'th stdout is passed to (i+1)'st stdin.
     * If `loop` is true, stdout of the last process is directed to stdin of the first one.
     *
     * Useful for judging interactive problems: run two commands with looping enabled:
     *
     *     Solution        Interactor
     *     --------        ----------
     *       stdin <--   --> stdin
     *                \ /
     *                 X
     *                / \
     *      stdout ---   --- stdout
     */
    struct TPipelineResult {
        std::vector<TResult> Results;
        std::vector<size_t> OrderOfTermination;
    };
    TPipelineResult RunPipeline(std::vector<TCommand> commands, bool loop, const ICancellationToken* ct = nullptr);

    /**
     * Checks the executable file validity,
     * but does not acually run the code (CREATE_SUSPENDED).
     * See https://codeforces.com/blog/entry/12609
     */
    bool CanRun(const TCommand& command);
}
