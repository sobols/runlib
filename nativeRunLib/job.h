#pragma once

#include "handle.h"
#include "process.h"
#include "metrics.h"

namespace NRunLib2 {
    // Completion key in WinAPI may have different types:
    // it is defined as PVOID in JOBOBJECT_ASSOCIATE_COMPLETION_PORT,
    // but as ULONG_PTR in CreateIoCompletionPort.
    // To avoid confusion, use a custom integer type.
    using TIocpKey = std::uintptr_t;

    class TIoCompletionPort {
    public:
        TIoCompletionPort();

        HANDLE Handle() const {
            return *Handle_;
        }

        struct TCompletionStatus {
            DWORD Code = {}; // one of JOB_OBJECT_MSG_...
            TIocpKey Key = {};
        };
        std::optional<TCompletionStatus> GetNext(DWORD dwMilliseconds);

    private:
        TScopedHandle Handle_;
    };

    class TJobObject {
    public:
        TJobObject();
        void SetLimits(std::optional<uint32_t> activeProcessLimit);
        void SetUiRestrictions();
        TIocpKey AssociateCompletionPort(TIoCompletionPort& cp);
        void Assign(TProcess& process);
        void Terminate(); // assumed to be asynchronous

        TCurrentTime GetTime() const;
        TCurrentMemory GetMemory() const;

    private:
        TScopedHandle Handle_;
    };

    TJobObject CreateJob(std::optional<uint32_t> activeProcessLimit);
}
