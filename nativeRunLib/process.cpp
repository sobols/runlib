#include "process.h"

#include "attribute_list.h"
#include "desktop.h"
#include "common.h"
#include "runlib2.h"
#include "security.h"
#include "redirect.h"

#include <Psapi.h>

#include <algorithm>

namespace NRunLib2 {
    namespace {
        std::wstring MakeEnvironmentBlock(const std::vector<TEnvVariable>& env);
        DWORD ConvertProcessPriorityClass(EProcessPriority p);

        class TCreateProcessOptions {
        public:
            TCreateProcessOptions(const TCommand& command)
                : RedirectHandles_{command.StdInput, command.StdOutput, command.StdError}
            {
                CommandLine_ = StdStringToStdWstring(command.CommandLine);
                if (command.CurrentDirectory) {
                    CurrentDirectory_ = StdStringToStdWstring(*command.CurrentDirectory);
                }
                if (command.EnvironmentMode == EEnvironmentMode::Override) {
                    Environment_ = MakeEnvironmentBlock(command.EnvVariables);
                }
                FillStartupInfo(command.GuiMode == EGuiMode::Hide);
                PriorityClass_ = ConvertProcessPriorityClass(command.ProcessPriority);
            }

            LPCWSTR ApplicationName() const {
                return NULL;
            }
            LPWSTR CommandLine() {
                return CommandLine_.data();
            }
            BOOL InheritHandles() {
                return TRUE;
            }
            DWORD CreationFlags() {
                return PriorityClass_ |
                    CREATE_NEW_PROCESS_GROUP |
                    CREATE_NEW_CONSOLE |
                    CREATE_SUSPENDED |
                    CREATE_UNICODE_ENVIRONMENT |
                    CREATE_BREAKAWAY_FROM_JOB |
                    EXTENDED_STARTUPINFO_PRESENT;
            }
            LPVOID Environment() {
                return Environment_ ? Environment_->data() : NULL;
            }
            LPCWSTR CurrentDirectory() {
                return CurrentDirectory_ ? CurrentDirectory_->data() : NULL;
            }
            LPSTARTUPINFOW StartupInfo() {
                return &StartupInfoEx_.StartupInfo;
            }

        private:
            void FillStartupInfo(bool hideWindow) {
                auto& ex = StartupInfoEx_;

                ::ZeroMemory(&ex, sizeof(ex));
                ex.StartupInfo.cb = sizeof(ex);
                ex.StartupInfo.dwFlags = STARTF_FORCEOFFFEEDBACK;
                if (hideWindow) {
                    ex.StartupInfo.dwFlags |= STARTF_USESHOWWINDOW;
                    ex.StartupInfo.wShowWindow = SW_HIDE;
                    ex.StartupInfo.lpDesktop = TWindowsDesktop::Instance().Name();
                }
                RedirectHandles_.Fill(ex.StartupInfo);

                EnableStdHandleInheritance(ex.StartupInfo, AttributeList_);
                ex.lpAttributeList = AttributeList_.Get();
            }

        private:
            std::wstring CommandLine_;
            std::optional<std::wstring> CurrentDirectory_;
            std::optional<std::wstring> Environment_;
            TRedirectHandles RedirectHandles_;
            TProcThreadAttributeList AttributeList_;
            STARTUPINFOEXW StartupInfoEx_;
            DWORD PriorityClass_;
        };

        TProcess LaunchProcess(TCreateProcessOptions& opts) {
            PROCESS_INFORMATION procInfo;
            if (!::CreateProcessW(
                opts.ApplicationName(),
                opts.CommandLine(),
                NULL,
                NULL,
                opts.InheritHandles(),
                opts.CreationFlags(),
                opts.Environment(),
                opts.CurrentDirectory(),
                opts.StartupInfo(),
                &procInfo))
            {
                throw TCreateProcessError{"CreateProcess"};
            }
            return TProcess{std::move(procInfo)};
        }

        TProcess LaunchProcessAtLowIntegrity(TCreateProcessOptions& opts) {
            // see https://docs.microsoft.com/en-us/previous-versions/dotnet/articles/bb625960(v=msdn.10)
            TScopedHandle hNewToken = MakeRestrictedToken();
            SetLowIntegrityLevel(*hNewToken);

            PROCESS_INFORMATION procInfo;
            if (!::CreateProcessAsUserW(*hNewToken,
                opts.ApplicationName(),
                opts.CommandLine(),
                NULL,
                NULL,
                opts.InheritHandles(),
                opts.CreationFlags(),
                opts.Environment(),
                opts.CurrentDirectory(),
                opts.StartupInfo(),
                &procInfo))
            {
                throw TCreateProcessError{"CreateProcessAsUser"};
            }
            return TProcess{std::move(procInfo)};
        }

        std::wstring MakeEnvironmentBlock(const std::vector<TEnvVariable>& env) {
            std::wstring buffer;
            for (const auto& var : env) {
                if (var.Name.find('=') != std::string::npos) {
                    throw TConfigurationError{"env variable name must not contain equality sign"};
                }
                if (var.Name.empty()) {
                    throw TConfigurationError{"empty env variable name"};
                }
                if (var.Name.find('\0') != std::string::npos || var.Value.find('\0') != std::string::npos) {
                    throw TConfigurationError{"env variable contains a null byte"};
                }
                buffer += StdStringToStdWstring(var.Name);
                buffer += L'=';
                buffer += StdStringToStdWstring(var.Value);
                buffer += L'\0';
            }
            buffer += L'\0';
            return buffer;
        }

        DWORD ConvertProcessPriorityClass(EProcessPriority p) {
            switch (p) {
            case EProcessPriority::Default:
                return 0;
            case EProcessPriority::RealTime:
                return REALTIME_PRIORITY_CLASS;
            case EProcessPriority::High:
                return HIGH_PRIORITY_CLASS;
            case EProcessPriority::AboveNormal:
                return ABOVE_NORMAL_PRIORITY_CLASS;
            case EProcessPriority::Normal:
                return NORMAL_PRIORITY_CLASS;
            case EProcessPriority::BelowNormal:
                return BELOW_NORMAL_PRIORITY_CLASS;
            case EProcessPriority::Idle:
                return IDLE_PRIORITY_CLASS;
            }
            throw TConfigurationError{"Invalid process priority"};
        }
    } // namespace

    TProcess LaunchSuspendedProcess(const TCommand& command) {
        TCreateProcessOptions opts{command};
        if (command.SecurityMode == ESecurityMode::LowIntegrity) {
            return LaunchProcessAtLowIntegrity(opts);
        }
        else {
            return LaunchProcess(opts);
        }
    }

    TCurrentMemory TProcess::GetMemory() const {
        PROCESS_MEMORY_COUNTERS_EX pmc = {};
        pmc.cb = sizeof(pmc);

        if (!::GetProcessMemoryInfo(Handle(), (PPROCESS_MEMORY_COUNTERS)&pmc, sizeof(pmc))) {
            throw TWindowsError{"GetProcessMemoryInfo"};
        }

        return {std::max(pmc.PeakPagefileUsage, pmc.PrivateUsage), pmc.PeakWorkingSetSize};
    }
}
