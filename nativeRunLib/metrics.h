#pragma once

#include "runlib2.h"

#include <Windows.h>

#include <cstdint>

namespace NRunLib2 {
    inline uint64_t ToUint64(const LARGE_INTEGER& li) {
        return static_cast<uint64_t>(li.QuadPart);
    }

    inline uint64_t ToUint64(const FILETIME& ft) {
        return ULARGE_INTEGER{ft.dwLowDateTime, ft.dwHighDateTime}.QuadPart;
    }

    struct TCurrentTime {
        // the number of 100-nanosecond intervals
        uint64_t UserTime;
        uint64_t KernelTime;

        uint64_t TotalTimeRaw() const {
            return UserTime + KernelTime;
        }

        uint64_t TotalTimeMs() const {
            return TotalTimeRaw() / 10000;
        }

        TCurrentTime()
            : UserTime{0}
            , KernelTime{0}
        {
        }

        TCurrentTime(const LARGE_INTEGER& user, const LARGE_INTEGER& kernel)
            : UserTime{ToUint64(user)}
            , KernelTime{ToUint64(kernel)}
        {
        }

        TCurrentTime(const FILETIME& user, const FILETIME& kernel)
            : UserTime{ToUint64(user)}
            , KernelTime{ToUint64(kernel)}
        {
        }
    };

    struct TCurrentMemory {
        uint64_t PeakCommitCharge;
        uint64_t PeakWorkingSetSize;

        uint64_t PeakBytes(EMemoryAccountingMode mode) const {
            return (mode == EMemoryAccountingMode::CommitCharge) ? PeakCommitCharge : PeakWorkingSetSize;
        }
    };

    /**
     * Detects the situations when a child process hangs
     * and does not consume CPU time.
     */
    class TIdleDetector {
    public:
        TIdleDetector(std::optional<uint64_t> timeLimit);

        // True means that the process is considered idle
        bool Check(const TCurrentTime&, uint64_t cycles);

    private:
        struct TState {
            uint64_t TickCount;
            uint64_t ProcessTime;
            uint64_t Cycles;
        };
        std::optional<uint64_t> HardTimeLimit_;
        uint64_t StartTickCount_;
        std::optional<TState> LastChange_;
    };
}
