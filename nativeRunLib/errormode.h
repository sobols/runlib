#pragma once

#include <stdint.h>

namespace NRunLib2 {
    /**
     * Disables "program.exe has stopped working" dialogs
     */
    class TErrorModeGuard {
    public:
        TErrorModeGuard();
        ~TErrorModeGuard();

    private:
        uint32_t Mode_;
    };

    void DisableErrorReportingDialog();
}
