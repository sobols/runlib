#include <nativeRunLib/common.h>
#include <nativeRunLib/exception.h>
#include <nativeRunLib/runlib2.h>

#include <Windows.h>

void ReportError(std::string_view what) {
    std::wstring message = StdStringToStdWstring(what);
    ::MessageBoxW(NULL, message.c_str(), NULL, MB_OK | MB_ICONERROR);
}

int APIENTRY WinMain(HINSTANCE hInstance,
    HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    NRunLib2::TCommand cmd{GetWinPath() + "\\notepad.exe"};
    cmd.TimeLimit = std::nullopt;
    cmd.IdlenessLimit = false;
    cmd.GuiMode = NRunLib2::EGuiMode::Normal;

    try {
        NRunLib2::Run(cmd);
        return 0;
    } catch (const TWindowsError& err) {
        ReportError(err.VerboseWhat());
        return 1;
    } catch (const std::exception& ex) {
        ReportError(ex.what());
        return 1;
    }
}
