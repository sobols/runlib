// This is the main DLL file.

#include "stdafx.h"

#include "CliRunLib2.h"

#pragma unmanaged
#include <nativeRunLib/cancel.h>
#include <nativeRunLib/filesystem.h>
#include <nativeRunLib/exception.h>
#pragma managed

#using <mscorlib.dll>
#include <vcclr.h>

namespace CliRunLib2 {
    namespace {
        class TCancellationTokenWrapper : public NRunLib2::ICancellationToken {
        public:
            TCancellationTokenWrapper(System::Threading::CancellationToken^ ct)
                : NetToken_{ct}
            {
            }
            bool IsCancellationRequested() const override {
                return NetToken_->IsCancellationRequested;
            }
            void Cancel() override {
                // not implemented
            }

        private:
            gcroot<System::Threading::CancellationToken ^> NetToken_;
        };

        template <class F>
        auto Wrap(F&& func) {
            try {
                return func();
            } catch (const TWindowsError& err) {
                throw gcnew RunException(err.VerboseWhat());
            } catch (const std::exception& exc) {
                throw gcnew RunException(exc.what());
            } catch (...) {
                throw gcnew RunException("Unknown error (...)");
            }
        }

        Result ToNetResult(const NRunLib2::TResult& resultImpl) {
            Result result;
            result.ExitCode = resultImpl.ExitCode;
            result.TimeUsed = resultImpl.TimeUsed;
            result.MemoryUsed = resultImpl.MemoryUsed;
            result.Outcome = OutcomeEnum(static_cast<int>(resultImpl.GetOutcome()));
            return result;
        }

        std::vector<NRunLib2::TCommand> ToNativeCommands(array<Command^>^ cmds) {
            std::vector<NRunLib2::TCommand> nativeCmds;
            nativeCmds.reserve(cmds->Length);
            for each (Command^ cmd in cmds) {
                nativeCmds.push_back(cmd->Impl());
            }
            return nativeCmds;
        }

        void CheckCanceled(const NRunLib2::TResult& resultImpl) {
            if (resultImpl.Canceled) {
                throw gcnew OperationCanceledException();
            }
        }

        Result RunImpl(const NRunLib2::TCommand& cmd, NRunLib2::ICancellationToken* ct) {
            const NRunLib2::TResult resultImpl = Wrap([&] {
                return NRunLib2::Run(cmd, ct);
            });
            CheckCanceled(resultImpl);
            return ToNetResult(resultImpl);
        }

        PipelineResult^ RunPipelineImpl(std::vector<NRunLib2::TCommand>&& cmds, bool loop, NRunLib2::ICancellationToken* ct) {
            const NRunLib2::TPipelineResult resultImpl = Wrap([&] {
                return NRunLib2::RunPipeline(std::move(cmds), loop, ct);
            });

            for (const auto& res : resultImpl.Results) {
                CheckCanceled(res);
            }

            PipelineResult^ result = gcnew PipelineResult();

            const int resultsSize = static_cast<int>(resultImpl.Results.size());
            result->Results = gcnew array<Result>(resultsSize);
            for (int i = 0; i < resultsSize; ++i) {
                result->Results[i] = ToNetResult(resultImpl.Results[i]);
            }

            const int orderOfTerminationSize = static_cast<int>(resultImpl.OrderOfTermination.size());
            result->OrderOfTermination = gcnew array<int>(orderOfTerminationSize);
            for (int i = 0; i < orderOfTerminationSize; ++i) {
                result->OrderOfTermination[i] = static_cast<int>(resultImpl.OrderOfTermination[i]);
            }
            return result;
        }

        void CreateDirectoryImpl(const std::wstring& wpath) {
            Wrap([&] { NRunLib2::CreateDirectoryAtLowIntegrity(wpath.c_str()); });
        }

        bool CanRunImpl(const NRunLib2::TCommand& cmd) {
            return Wrap([&]{ return NRunLib2::CanRun(cmd); });
        }
    }

    void Runner::CreateDirectory(String^ path) {
        std::wstring wpath = StdStringToStdWstring(NPrivate::ManagedToStl(path));
        CreateDirectoryImpl(wpath);
    }

    Result Runner::Run(Command^ cmd) {
        return RunImpl(cmd->Impl(), nullptr);
    }

    Result Runner::Run(Command^ cmd, System::Threading::CancellationToken ct) {
        TCancellationTokenWrapper ctWrapper{ct};
        return RunImpl(cmd->Impl(), &ctWrapper);
    }

    PipelineResult^ Runner::RunPipeline(array<Command^>^ cmds, bool loop) {
        return RunPipelineImpl(ToNativeCommands(cmds), loop, nullptr);
    }

    PipelineResult^ Runner::RunPipeline(array<Command^>^ cmds, bool loop, System::Threading::CancellationToken ct) {
        TCancellationTokenWrapper ctWrapper{ct};
        return RunPipelineImpl(ToNativeCommands(cmds), loop, &ctWrapper);
    }

    bool Runner::CanRun(Command^ cmd) {
        return CanRunImpl(cmd->Impl());
    }
}
