// cliRunLib.h

#pragma once

#include "private.h"

#pragma unmanaged
#include <nativeRunLib/common.h>
#include <nativeRunLib/runlib.h>
#include <sstream>
#pragma managed

using namespace System;

namespace CliRunLib {
    public ref class Command
    {
    public:
        enum class SecurityModeEnum
        {
            None,
            DropRights,
            CustomUser,
            CustomUserDropRights
        };
        enum class MemoryAccountingModeEnum
        {
            CommitCharge,
            WorkingSetSize
        };
    public:
        Command()
        {
            m_impl = new NRunLib::TCommand();
        }
        ~Command()
        {
            this->!Command();
        }
        !Command()
        {
            delete m_impl;
            m_impl = NULL;
        }

        property String^ CommandLine
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->CommandLine);
            }
            void set(String^ data)
            {
                m_impl->CommandLine = NPrivate::ManagedToStlOpt(data);
            }
        }
        property String^ Directory
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->Directory);
            }
            void set(String^ data)
            {
                m_impl->Directory = NPrivate::ManagedToStlOpt(data);
            }
        }

        property Nullable<long long> TimeLimit
        {
            Nullable<long long> get()
            {
                return (m_impl->TimeLimit) ? Nullable<long long>(*m_impl->TimeLimit) : Nullable<long long>();
            }
            void set(Nullable<long long> data)
            {
                m_impl->TimeLimit = data.HasValue ? std::optional<long long>(data.Value) : std::optional<long long>();
            }
        }
        property Nullable<long long> MemoryLimit
        {
            Nullable<long long> get()
            {
                return (m_impl->MemoryLimit) ? Nullable<long long>(*m_impl->MemoryLimit) : Nullable<long long>();
            }
            void set(Nullable<long long> data)
            {
                m_impl->MemoryLimit = data.HasValue ? std::optional<long long>(data.Value) : std::optional<long long>();
            }
        }

        property String^ StdInFile
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->StdInFile);
            }
            void set(String^ data)
            {
                m_impl->StdInFile = NPrivate::ManagedToStlOpt(data);
            }
        }
        property String^ StdOutFile
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->StdOutFile);
            }
            void set(String^ data)
            {
                m_impl->StdOutFile = NPrivate::ManagedToStlOpt(data);
            }
        }
        property String^ StdErrFile
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->StdErrFile);
            }
            void set(String^ data)
            {
                m_impl->StdErrFile = NPrivate::ManagedToStlOpt(data);
            }
        }

        property SecurityModeEnum SecurityMode
        {
            SecurityModeEnum get()
            {
                return SecurityModeEnum(static_cast<int>(m_impl->SecurityMode));
            }
            void set(SecurityModeEnum data)
            {
                m_impl->SecurityMode = static_cast<NRunLib::TCommand::ESecurityMode>((int)data);
            }
        }
        property MemoryAccountingModeEnum MemoryAccountingMode
        {
            MemoryAccountingModeEnum get()
            {
                return MemoryAccountingModeEnum(static_cast<int>(m_impl->MemoryAccountingMode));
            }
            void set(MemoryAccountingModeEnum data)
            {
                m_impl->MemoryAccountingMode = static_cast<NRunLib::TCommand::EMemoryAccountingMode>((int)data);
            }
        }


        property String^ AuthorizationLogin
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->AuthorizationParams.Login);
            }
            void set(String^ data)
            {
                m_impl->AuthorizationParams.Login = NPrivate::ManagedToStlOpt(data);
            }
        }
        property String^ AuthorizationPassword
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->AuthorizationParams.Password);
            }
            void set(String^ data)
            {
                m_impl->AuthorizationParams.Password = NPrivate::ManagedToStlOpt(data);
            }
        }
        property String^ AuthorizationDomain
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->AuthorizationParams.Domain);
            }
            void set(String^ data)
            {
                m_impl->AuthorizationParams.Domain = NPrivate::ManagedToStlOpt(data);
            }
        }

        property bool IsTrustedProcess
        {
            bool get()
            {
                return m_impl->IsTrustedProcess;
            }
            void set(bool data)
            {
                m_impl->IsTrustedProcess = data;
            }
        }
        property bool IsIdlenessCheck
        {
            bool get()
            {
                return m_impl->IsIdlenessChecking;
            }
            void set(bool data)
            {
                m_impl->IsIdlenessChecking = data;
            }
        }
        property bool IsEnvironmentOverridden
        {
            bool get()
            {
                return m_impl->IsEnvironmentOverridden;
            }
            void set(bool data)
            {
                m_impl->IsEnvironmentOverridden = data;
            }
        }
        virtual String^ ToString() override
        {
            std::ostringstream oss;
            oss << *m_impl;
            return NPrivate::StlToManaged(oss.str());
        }

        // no getter yet
        void AddEnvironmentVariable(String^ name, String^ value)
        {
            if (name == nullptr || value == nullptr) {
                return;
            }
            m_impl->EnvironmentVariables.push_back(std::make_pair(*NPrivate::ManagedToStlOpt(name), *NPrivate::ManagedToStlOpt(value)));
        }

    internal:
        const NRunLib::TCommand* GetImpl()
        {
            return m_impl;
        }
    private:
        NRunLib::TCommand* m_impl;
    };

    public ref class Result
    {
    public:
        enum class OutcomeEnum
        {
            Ok,
            TimeLimitExceeded,
            MemoryLimitExceeded,
            IdlenessLimitExceeded,
            RuntimeError,
            SecurityViolation,
            Fail
        };

    public:
        Result()
        {
            m_impl = new NRunLib::TResult();
        }
        ~Result()
        {
            this->!Result();
        }
        !Result()
        {
            delete m_impl;
            m_impl = NULL;
        }

        property OutcomeEnum Outcome
        {
            OutcomeEnum get()
            {
                return OutcomeEnum(static_cast<int>(m_impl->Outcome));
            }
            void set(OutcomeEnum data)
            {
                m_impl->Outcome = static_cast<NRunLib::TResult::EOutcome>((int)data);
            }
        }
        property long long TimeUsed
        {
            long long get()
            {
                return m_impl->TimeUsed;
            }
            void set(long long data)
            {
                m_impl->TimeUsed = data;
            }
        }
        property long long MemoryUsed
        {
            long long get()
            {
                return m_impl->MemoryUsed;
            }
            void set(long long data)
            {
                m_impl->MemoryUsed = data;
            }
        }
        property int ExitCode
        {
            int get()
            {
                return m_impl->ExitCode;
            }
            void set(int data)
            {
                m_impl->ExitCode = data;
            }
        }
        property String^ Message
        {
            String^ get()
            {
                return NPrivate::StlToManaged(m_impl->Message);
            }
            void set(String^ data)
            {
                m_impl->Message = NPrivate::ManagedToStlOpt(data);
            }
        }
        virtual String^ ToString() override
        {
            std::ostringstream oss;
            oss << *m_impl;
            return NPrivate::StlToManaged(oss.str());
        }
    internal:
        Result(const NRunLib::TResult& obj)
        {
            m_impl = new NRunLib::TResult(obj);
        }
    private:
        NRunLib::TResult* m_impl;
    };

    public ref class Runner
    {
    public:
        // simple static function, blocks until result is ready
        static Result^ Run(Command^ command)
        {
            if (!command) {
                return nullptr;
            }
            return gcnew Result(NRunLib::Run(*command->GetImpl()));
        }

        Runner(Command^ command)
        {
            m_impl = new NRunLib::TRunner(*command->GetImpl());
        }
        ~Runner()
        {
            this->!Runner();
        }
        !Runner()
        {
            delete m_impl;
            m_impl = NULL;
        }

        Result^ WaitForResult()
        {
            const std::optional<NRunLib::TResult> result = m_impl->WaitForResult();
            return result ? gcnew Result(*result) : nullptr;
        }
        Result^ WaitForResult(int timeout)
        {
            const std::optional<NRunLib::TResult> result = m_impl->WaitForResult(timeout);
            return result ? gcnew Result(*result) : nullptr;
        }
        void Kill()
        {
            m_impl->Kill();
        }
    private:
        NRunLib::TRunner* m_impl;
    };
}
