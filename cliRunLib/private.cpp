#include "stdafx.h"

#include "private.h"

#pragma unmanaged
#include <nativeRunLib/common.h>
#pragma managed

#include <vcclr.h>

using namespace System;

namespace NPrivate {
    std::optional<std::string> ManagedToStlOpt(String^ s)
    {
        if (s == nullptr)
        {
            return std::nullopt;
        }
        return ManagedToStl(s);
    }

    std::string ManagedToStl(String^ s)
    {
        if (s == nullptr)
        {
            Exception^ ex = gcnew ArgumentNullException("string");
            throw ex;
        }
        pin_ptr<const wchar_t> pinchars = PtrToStringChars(s);
        const size_t length = static_cast<size_t>(s->Length);
        return WideCharToStdString({pinchars, length});
    }

    String^ StlToManaged(const std::string& s)
    {
        const int length = static_cast<int>(s.length());
        array<unsigned char>^ bytes = gcnew array<unsigned char>(length);
        pin_ptr<unsigned char> pinnedPtr = &bytes[0];
        memcpy(pinnedPtr, s.data(), s.length());
        return Text::Encoding::UTF8->GetString(bytes);
    }

    String^ StlToManaged(const std::optional<std::string>& s)
    {
        if (!s.has_value())
        {
            return nullptr;
        }
        return StlToManaged(*s);
    }
} // namespace NPrivate
