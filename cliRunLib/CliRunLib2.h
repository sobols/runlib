// cliRunLib.h

#pragma once

#include "private.h"

#pragma unmanaged
#include <nativeRunLib/common.h>
#include <nativeRunLib/runlib2.h>
#include <sstream>
#pragma managed

using namespace System;

namespace CliRunLib2 {
    public enum class MemoryAccountingModeEnum {
        CommitCharge,
        WorkingSetSize
    };

    public enum class LimitAccountingModeEnum {
        PerProcess,
        PerJob,
    };

    public enum class ProcessPriorityEnum {
        Default,
        RealTime,
        High,
        AboveNormal,
        Normal,
        BelowNormal,
        Idle,
    };

    public ref class Command {
    public:
        Command(String^ commandLine)
            : m_impl{new NRunLib2::TCommand{*NPrivate::ManagedToStlOpt(commandLine)}}
        {
        }
        ~Command() {
            this->!Command();
        }
        !Command() {
            delete m_impl;
            m_impl = nullptr;
        }

        property String^ CommandLine {
            String^ get() {
                return NPrivate::StlToManaged(m_impl->CommandLine);
            }
            void set(String^ data) {
                m_impl->CommandLine = *NPrivate::ManagedToStlOpt(data);
            }
        }

        property String^ CurrentDirectory {
            String^ get() {
                return NPrivate::StlToManaged(m_impl->CurrentDirectory);
            }
            void set(String^ data) {
                m_impl->CurrentDirectory = NPrivate::ManagedToStlOpt(data);
            }
        }

        property Nullable<uint64_t> TimeLimit {
            Nullable<uint64_t> get() {
                return NPrivate::ToManaged(m_impl->TimeLimit);
            }
            void set(Nullable<uint64_t> data) {
                m_impl->TimeLimit = NPrivate::ToNative(data);
            }
        }

        property Nullable<uint64_t> MemoryLimit {
            Nullable<uint64_t> get() {
                return NPrivate::ToManaged(m_impl->MemoryLimit);
            }
            void set(Nullable<uint64_t> data) {
                m_impl->MemoryLimit = NPrivate::ToNative(data);
            }
        }

        property Nullable<uint32_t> ProcessLimit {
            Nullable<uint32_t> get() {
                return NPrivate::ToManaged(m_impl->ProcessLimit);
            }
            void set(Nullable<uint32_t> data) {
                m_impl->ProcessLimit = NPrivate::ToNative(data);
            }
        }

        property uint64_t AffinityMask {
            uint64_t get() {
                return m_impl->AffinityMask;
            }
            void set(uint64_t data) {
                m_impl->AffinityMask = data;
            }
        }

        property bool IdlenessLimit {
            bool get() {
                return m_impl->IdlenessLimit;
            }
            void set(bool data) {
                m_impl->IdlenessLimit = data;
            }
        }

        void RedirectStdInputFrom(String^ path) {
            if (auto f = NPrivate::ManagedToStlOpt(path)) {
                m_impl->StdInput.SetFile(*f);
            }
            else {
                m_impl->StdInput.SetNull();
            }
        }
        void RedirectStdOutputTo(String^ path) {
            if (auto f = NPrivate::ManagedToStlOpt(path)) {
                m_impl->StdOutput.SetFile(*f);
            }
            else {
                m_impl->StdOutput.SetNull();
            }
        }
        void RedirectStdErrorTo(String^ path) {
            if (auto f = NPrivate::ManagedToStlOpt(path)) {
                m_impl->StdError.SetFile(*f);
            }
            else {
                m_impl->StdError.SetNull();
            }
        }
        void RedirectAllTo(String^ path) {
            if (auto f = NPrivate::ManagedToStlOpt(path)) {
                m_impl->StdOutput.SetFile(*f);
                m_impl->StdError.Mode = NRunLib2::ERedirectMode::StdOutput;
            }
            else {
                m_impl->StdOutput.SetNull();
                m_impl->StdError.SetNull();
            }
        }

        property MemoryAccountingModeEnum MemoryAccountingMode
        {
            MemoryAccountingModeEnum get() {
                return MemoryAccountingModeEnum(static_cast<int>(m_impl->MemoryAccountingMode));
            }
            void set(MemoryAccountingModeEnum data) {
                m_impl->MemoryAccountingMode = static_cast<NRunLib2::EMemoryAccountingMode>((int)data);
            }
        }

        property LimitAccountingModeEnum LimitAccountingMode
        {
            LimitAccountingModeEnum get() {
                return LimitAccountingModeEnum(static_cast<int>(m_impl->LimitAccountingMode));
            }
            void set(LimitAccountingModeEnum data) {
                m_impl->LimitAccountingMode = static_cast<NRunLib2::ELimitAccountingMode>((int)data);
            }
        }

        property ProcessPriorityEnum ProcessPriority
        {
            ProcessPriorityEnum get() {
                return ProcessPriorityEnum(static_cast<int>(m_impl->ProcessPriority));
            }
            void set(ProcessPriorityEnum data) {
                m_impl->ProcessPriority = static_cast<NRunLib2::EProcessPriority>((int)data);
            }
        }

        property bool IsEnvironmentOverridden {
            bool get() {
                return m_impl->EnvironmentMode == NRunLib2::EEnvironmentMode::Override;
            }
            void set(bool value) {
                m_impl->EnvironmentMode = value ? NRunLib2::EEnvironmentMode::Override : NRunLib2::EEnvironmentMode::Inherit;
            }
        }

        // no getter yet
        void AddEnvironmentVariable(String^ name, String^ value) {
            m_impl->EnvVariables.push_back({NPrivate::ManagedToStl(name), NPrivate::ManagedToStl(value)});
        }

    internal:
        const NRunLib2::TCommand& Impl() {
            return *m_impl;
        }

    private:
        NRunLib2::TCommand* m_impl;
    };

    public enum class OutcomeEnum {
        Ok,
        TimeLimitExceeded,
        MemoryLimitExceeded,
        IdlenessLimitExceeded,
        RuntimeError,
        SecurityViolation,
    };

    public value class Result {
    public:
        UInt64 TimeUsed;
        UInt64 MemoryUsed;
        UInt32 ExitCode;
        OutcomeEnum Outcome;
    };

    public ref class PipelineResult {
    public:
        array<Result>^ Results;
        array<int>^ OrderOfTermination;
    };

    public ref struct RunException : public System::Exception {
    public:
        RunException(const std::string& msg)
            : Exception(NPrivate::StlToManaged(msg))
        {
        }
    };

    public ref class Runner abstract sealed {
    public:
        static void CreateDirectory(String^ path);
        static Result Run(Command^ cmd);
        static Result Run(Command^ cmd, System::Threading::CancellationToken ct);
        static PipelineResult^ RunPipeline(array<Command^>^ cmds, bool loop);
        static PipelineResult^ RunPipeline(array<Command^>^ cmds, bool loop, System::Threading::CancellationToken ct);
        static bool CanRun(Command^ cmd);
    };
}
