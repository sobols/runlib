#pragma once

#pragma unmanaged
#include <optional>
#include <string>
#pragma managed

namespace NPrivate {
    std::optional<std::string> ManagedToStlOpt(System::String^ s);
    std::string ManagedToStl(System::String^ s);
    System::String^ StlToManaged(const std::string& s);
    System::String^ StlToManaged(const std::optional<std::string>& s);

    template <class T>
    System::Nullable<T> ToManaged(std::optional<T> x) {
        return x.has_value() ? Nullable<T>(*x) : Nullable<T>();
    }

    template <class T>
    std::optional<T> ToNative(System::Nullable<T> x) {
        return x.HasValue ? std::optional<T>(x.Value) : std::optional<T>();
    }
}
