//  Decompiled by jdec
//  DECOMPILER HOME PAGE: jdec.sourceforge.net
//  Main HOSTING SITE: sourceforge.net
//  Copyright (C)2006,2007,2008 Swaroop Belur.
//  jdec comes with ABSOLUTELY NO WARRANTY;
//  This is free software, and you are welcome to redistribute
//  it under certain conditions;
//  See the File 'COPYING' For more details.

package com.irunner.runner;
 
/**** List of All Imported Classes ***/

import com.irunner.runner.ExecutionCommand;
import com.irunner.runner.ExecutionResult;
// import com.irunner.runner.Tester;
// import com.katrin.IRunnerSystem;
// import com.katrin.logging.Location;
// import com.katrin.logging.Severity;
import java.io.File;
import java.io.PrintStream;
import java.lang.Object;
import java.lang.String;
import java.lang.System;
import java.lang.UnsatisfiedLinkError;

// End of Import

public  class  SafeRunner  

{

 /***
 **Class Fields
 ***/
 private  static  boolean  inited ;


//  CLASS: com.irunner.runner.SafeRunner:
 public    SafeRunner( ) 
 {
    super();
    return;

 }

//  CLASS: com.irunner.runner.SafeRunner:
 public  static   boolean init( ) 
 {
    String string1= null;
    String string2= null;
    int int1= 0;
    if(SafeRunner.inited==false)
    {
      try
      {
        string1="newSafeRunner"; // IRunnerSystem.getProperty("executor.library");
        System.loadLibrary(string1);
        int1=0; // Severity.parse(IRunnerSystem.getProperty("executor.logSeverity"));
        string2="safeRunner.log"; // IRunnerSystem.getProperty("executor.logFile");
        initialize(string2,int1);
        SafeRunner.inited = true;
    
      }
      catch(UnsatisfiedLinkError  ex)
      {
        // Tester.logger.fatalT("Unable to find/initialize saferunner library");
        System.err.println("Unable to find/initialize saferunner library");
        ex.printStackTrace(System.err);
        SafeRunner.inited = false;
   
    }
      }
    return SafeRunner.inited;

 }

//  CLASS: com.irunner.runner.SafeRunner:
 public  static   ExecutionResult run(String string5,String string6,int int3) 
 {
    return run(string5,string6,int3,0,null,null);

 }

//  CLASS: com.irunner.runner.SafeRunner:
 // public  static ExecutionResult run(String commandLine, String path, int timeLimit, int memoryLimit, String redirectStdin, String redirectStdout) 
 public  static   ExecutionResult run(String string11,String string12,int int6,int int7,String string13,String string14) 
 {
    ExecutionCommand aExecutionCommand1= null;

    ExecutionCommand JdecGenerated2 = new ExecutionCommand();
    aExecutionCommand1=JdecGenerated2;
    aExecutionCommand1.commandLine =string11;
    File JdecGenerated19 = new File(string12);
    aExecutionCommand1.directory =JdecGenerated19.getAbsolutePath();
    aExecutionCommand1.timeLimit =int6;
    if(int7 > 0)
    {
      aExecutionCommand1.memoryLimit =int7;
      aExecutionCommand1.flag =(aExecutionCommand1.flag|1);
   
    }
    if(string13!=null && string13.trim().length()> 0)
  
    {
      aExecutionCommand1.flag =(aExecutionCommand1.flag|4);
      aExecutionCommand1.stdInFile =string13.trim();
   
    }
    if(string14!=null && string14.trim().length()> 0)
  
    {
      aExecutionCommand1.flag =(aExecutionCommand1.flag|8);
      aExecutionCommand1.stdOutFile =string14.trim();
   
    }
    // if(Tester.logger.beDebug()!=false)
    // {
    //   Tester.logger.debugT(aExecutionCommand1.toString());   
    // }
    return run(aExecutionCommand1);

 }

//  CLASS: com.irunner.runner.SafeRunner:
 static  native   ExecutionResult run(com.irunner.runner.ExecutionCommand aExecutionCommand2);

//  CLASS: com.irunner.runner.SafeRunner:
 public  static  native   void initialize(java.lang.String string15,int int8);

//  CLASS: com.irunner.runner.SafeRunner:
 static      //[Static Initializer] 
 {

    SafeRunner.inited = false;

 }


}
