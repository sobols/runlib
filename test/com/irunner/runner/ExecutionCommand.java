//  Decompiled by jdec
//  DECOMPILER HOME PAGE: jdec.sourceforge.net
//  Main HOSTING SITE: sourceforge.net
//  Copyright (C)2006,2007,2008 Swaroop Belur.
//  jdec comes with ABSOLUTELY NO WARRANTY;
//  This is free software, and you are welcome to redistribute
//  it under certain conditions;
//  See the File 'COPYING' For more details.

package com.irunner.runner;
 
/**** List of All Imported Classes ***/

import java.lang.Object;
import java.lang.String;
import java.lang.StringBuffer;

// End of Import

public  class  ExecutionCommand  

{

 /***
 **Class Fields
 ***/
 public  static  final  int  EC_NONE =0;
 public  static  final  int  EC_USE_MEMORY_LIMIT =1;
 public  static  final  int  EC_USE_TIME_LIMIT =2;
 public  static  final  int  EC_STD_IN_REDIRECT =4;
 public  static  final  int  EC_STD_OUT_REDIRECT =8;
 public  String  commandLine ;
 public  String  directory ;
 public  int  timeLimit ;
 public  int  memoryLimit ;
 public  String  stdInFile ;
 public  String  stdOutFile ;
 public  int  flag ;


//  CLASS: com.irunner.runner.ExecutionCommand:
 ExecutionCommand( ) 
 {
    super();
    this.flag =2;
    return;

 }

//  CLASS: com.irunner.runner.ExecutionCommand:
 public   String toString( ) 
 {

    StringBuffer JdecGenerated2 = new StringBuffer();
    return JdecGenerated2.append("cmdLine=").append(this.commandLine).append(" directory=").append(this.directory).append(" timeLimit=").append(this.timeLimit).append(" memoryLimit=").append(this.memoryLimit).toString();

 }


}