//  Decompiled by jdec
//  DECOMPILER HOME PAGE: jdec.sourceforge.net
//  Main HOSTING SITE: sourceforge.net
//  Copyright (C)2006,2007,2008 Swaroop Belur.
//  jdec comes with ABSOLUTELY NO WARRANTY;
//  This is free software, and you are welcome to redistribute
//  it under certain conditions;
//  See the File 'COPYING' For more details.

package com.irunner.runner;
 
/**** List of All Imported Classes ***/

import java.lang.Object;
import java.lang.String;
import java.lang.StringBuffer;

// End of Import

public  class  ExecutionResult  

{

 /***
 **Class Fields
 ***/
 public  static  final  int  RC_NO_ERROR =0;
 public  static  final  int  RC_RUNTIME_ERROR =1;
 public  static  final  int  RC_TIME_LIMIT_EXCEED =2;
 public  static  final  int  RC_MEMORY_LIMIT_EXCEED =3;
 public  static  final  int  RC_UNKNOWN_ERROR =4;
 public  int  result_code ;
 public  int  time_used ;
 public  int  memory_used ;
 public  int  runtime_error ;
 public  String  error_message ;


//  CLASS: com.irunner.runner.ExecutionResult:
 public    ExecutionResult( ) 
 {
    super();
    return;

 }

//  CLASS: com.irunner.runner.ExecutionResult:
 public   String toString( ) 
 {

    StringBuffer JdecGenerated2 = new StringBuffer();
    return JdecGenerated2.append("Execution Result:error code=").append(this.result_code).append(" time=").append(this.time_used).append(" memory pick=").append(this.memory_used).append(" runtime error=").append(this.runtime_error).append(" error message=").append(this.error_message).toString();

 }


}