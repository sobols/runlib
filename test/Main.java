import com.irunner.runner.SafeRunner;
import com.irunner.runner.ExecutionResult;
import java.io.File;

public class Main {

    static void test(String command) {
        final int timeLimit = 1000;
        final int memoryLimit = 64 * 1024 * 1024;

        //public static ExecutionResult run(String commandLine, String path, int timeLimit, int memoryLimit, String redirectStdin, String redirectStdout)
        ExecutionResult result = SafeRunner.run(command, "samples", timeLimit, memoryLimit, null, null);
        //System.out.println(result);

        String outcome = null;
        final int code = result.result_code;
        if (code == ExecutionResult.RC_NO_ERROR) {
            outcome = "OK";
        } else if (code == ExecutionResult.RC_RUNTIME_ERROR) {
            outcome = "RE";
        } else if (code == ExecutionResult.RC_TIME_LIMIT_EXCEED) {
            outcome = "TL";
        } else if (code == ExecutionResult.RC_MEMORY_LIMIT_EXCEED) {
            outcome = "ML";
        } else if (code == ExecutionResult.RC_UNKNOWN_ERROR) {
            outcome = "FAIL";
        };

        System.out.println(String.format("%-50s %5s   %5d ms    %7.2f MB    ret = %3d    %s", command, outcome, result.time_used, result.memory_used * 1.0 / (1024 * 1024), result.runtime_error, result.error_message));
    }

    static void sequence() {
        test("samples\\cases.exe");
        test("samples\\cases.exe return_42");
        test("samples\\cases.exe infinite_loop");
        test("samples\\cases.exe run_for_1100");
        test("samples\\cases.exe run_for_1500");
        test("samples\\cases.exe exception");
        test("samples\\cases.exe huge_array_unused");
        test("samples\\cases.exe huge_array_used");
        test("samples\\cases.exe run_self_tl");
        test("samples\\cases.exe run_self_ml");
        test("samples\\cases.exe clipboard_write");
        // test("samples\\cases.exe run_self_bomb"); // Dangerous!
        //test("calc.exe");     
    }

    static void nearTimeLimit() {
        for (int i = 900; i < 2000; i += 10) {
            test(String.format("samples\\cases.exe run_for_%d", i));
        }
    }

    static void nearMemoryLimit() {
        final int min = 57;
        final int max = 68;

        for (int i = min; i < max; i += 1) {
            test(String.format("samples\\cases.exe allocate_heap_%d", i * 1024 * 1024));
        }
        for (int i = min; i < max; i += 1) {
            test(String.format("samples\\cases.exe allocate_stack_%d", i * 1024 * 1024));
        }
        for (int i = min; i < max; i += 1) {
            test(String.format("samples\\cases.exe allocate_valloc_reserve_%d", i * 1024 * 1024));
        }
        for (int i = min; i < max; i += 1) {
            test(String.format("samples\\cases.exe allocate_valloc_use_%d", i * 1024 * 1024));
        }
    }

    public static void main(String[] args) {
        SafeRunner.init();

        sequence();
        //nearTimeLimit();
        //nearMemoryLimit();
    }
}
