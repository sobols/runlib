#include <iostream>
#include <string>
#include <sstream>
#include <cstring>
#include <cstdlib>
#include <Windows.h>

bool parsePrefixed(const std::string& cmd, const std::string& prefix, size_t* out) {
    if (cmd.substr(0, prefix.length()) == prefix) {
        std::istringstream iss(cmd.substr(prefix.length()));
        iss >> *out;
        return true;
    }
    return false;
}

int main(int argc, char *argv[]) {
    if (argc > 1) {
        const std::string cmd = argv[1];
        const int N = 100000000; // 100 M
        size_t target = 0;

        if (parsePrefixed(cmd, "return_", &target)) {
            return target;

        } else if (cmd == "infinite_loop") {
            while (true);

        } else if (parsePrefixed(cmd, "run_for_", &target)) {
            DWORD start = GetTickCount();
            volatile int x = 0;
            while (true) {
                for (int i = 0; i < 1000000; ++i) {
                    x += i;
                }
                DWORD cur = GetTickCount();
                if (cur - start > target) {
                    return 0;
                }
            }

        } else if (parsePrefixed(cmd, "allocate_heap_", &target)) {
            int target = atoi(cmd.substr(14).c_str());
            char* s = new char[target];
            memset(s, 0, target);
            delete [] s;

        } else if (parsePrefixed(cmd, "allocate_stack_", &target)) {
            int target = atoi(cmd.substr(15).c_str());
            char s[target];
            memset(s, 0, target);

        } else if (parsePrefixed(cmd, "allocate_valloc_reserve_", &target)) {
            void* s = VirtualAlloc(0, target, MEM_RESERVE, PAGE_READWRITE);

        } else if (parsePrefixed(cmd, "allocate_valloc_commit_", &target)) {
            void* s = VirtualAlloc(0, target,  MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);

        } else if (parsePrefixed(cmd, "allocate_valloc_use_", &target)) {
            char* s = (char*)VirtualAlloc(0, target,  MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
            for (size_t i = 0; i < target; ++i) {
                s[i] = i % 128;
            }

        } else if (cmd == "exception") {
            throw 1;

        } else if (cmd == "huge_array_unused") {
            int* a = new int[N];
            delete [] a;

        } else if (cmd == "huge_array_used") {
            int* a = new int[N];
            for (int i = 0; i < N; ++i) {
                a[i] = i;
            }
            delete [] a;

        } else if (cmd == "run_self_tl") {
            system("cases.exe infinite_loop");

        } else if (cmd == "run_self_ml") {
            system("cases.exe huge_array_used");

        } else if (cmd == "run_self_bomb") {
            system("cases.exe run_self_bomb");

        } else if (cmd == "clipboard_write") {
            const char* output = "Hacked";
            const size_t len = strlen(output) + 1;
            HGLOBAL hMem =  GlobalAlloc(GMEM_MOVEABLE, len);
            memcpy(GlobalLock(hMem), output, len);
            GlobalUnlock(hMem);
            OpenClipboard(0);
            EmptyClipboard();
            SetClipboardData(CF_TEXT, hMem);
            CloseClipboard();
        }
    }
}
