﻿using CliRunLib2;
using System;
using System.Threading;

namespace UseRunLibFromNET
{
    internal class Program
    {
        private static void UseV1()
        {
            CliRunLib.Command command = new CliRunLib.Command();
            command.CommandLine = "notepad.exe";
            command.MemoryAccountingMode = CliRunLib.Command.MemoryAccountingModeEnum.WorkingSetSize;
            command.TimeLimit = 1000;
            command.AddEnvironmentVariable("aba", "привет");
            command.AddEnvironmentVariable("name", "value");
            command.IsTrustedProcess = true;
            command.SecurityMode = CliRunLib.Command.SecurityModeEnum.None;
            /*command.IsIdlenessCheck = true;

            Console.WriteLine(command.ToString());
            CliRunLib.Result result = CliRunLib.Runner.Run(command);
            Console.WriteLine(result.ToString());
            */

            command.IsIdlenessCheck = false;
            CliRunLib.Runner runner = new CliRunLib.Runner(command);
            for (int i = 0; i < 10; ++i)
            {
                //if (i == 7)
                //{
                //  runner.Kill();
                //}
                CliRunLib.Result result = runner.WaitForResult(1000);
                Console.WriteLine(String.Format("{0}: {1}", i, (result != null) ? result.ToString() : "none"));
            }
            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static void UseV2()
        {
            var cts = new CancellationTokenSource();

            try
            {
                CliRunLib2.Runner.Run(new CliRunLib2.Command("does_not_exist.exe"));
            }
            catch (CliRunLib2.RunException ex)
            {
                Console.WriteLine(String.Format("Got exception '{0}'", ex.Message));
            }

            (new Thread(() => {
                Thread.Sleep(300);
                Console.WriteLine("Cancel!");
                cts.Cancel();
            })).Start();
            try
            {
                CliRunLib2.Runner.Run(new CliRunLib2.Command("notepad.exe"), cts.Token);
            }
            catch (OperationCanceledException ex)
            {
                Console.WriteLine(String.Format("Got exception '{0}'", ex.Message));
            }

            var command = new CliRunLib2.Command("notepad.exe");
            command.TimeLimit = 500;

            var res = CliRunLib2.Runner.Run(command);
            Console.WriteLine(res.Outcome);
            Console.WriteLine(String.Format("Time: {0}", res.TimeUsed));
            Console.WriteLine(String.Format("Memory: {0}", res.MemoryUsed));
            Console.WriteLine(String.Format("Exit code: {0}", res.ExitCode));
            Console.WriteLine("Press any key to continue");
            Console.ReadLine();
        }

        private static void UseV2Pipeline()
        {
            using (Command cmd1 = new Command("notepad.exe"))
            {
                using (Command cmd2 = new Command("notepad.exe"))
                {
                    var res = Runner.RunPipeline(new Command[] { cmd1, cmd2 }, false);
                }
            }
        }

        private static void Main(string[] args)
        {
            Console.WriteLine("Hello world");
            // UseV1();
            // UseV2();
            UseV2Pipeline();
            Console.WriteLine("Bye!");
        }
    }
}
