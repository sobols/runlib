RunLib
------

[![Build status](https://ci.appveyor.com/api/projects/status/uoyui0764ab7kccn?svg=true)](https://ci.appveyor.com/project/sobols/runlib)

Libraries for running programs in testing systems.

For building you need

  * Microsoft Visual Studio 2017 (the solution includes C, C\+\+, C\+\+/CLI and C# source code).

The solution includes three projects:

1. **nativeRunLib**. The main static library, see `runlib.h` for details. It has an easy-to-use C++ interface.
2. **cliRunLib**. Wrapper around nativeRunLib to be used in .NET applications. First of all, it was made for [UCode.Worker].
3. **useRunLibFromNET**. Example of using cliRunLib form C# code.

The nativeRunLib is based on [runlib-win32] by Paul P. Komkoff Jr.

  [UCode.Worker]: https://bitbucket.org/sobols/ucode.worker/
  [runlib-win32]: https://gitorious.org/contester/runlib-win32/
